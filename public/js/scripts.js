// Smartresize
(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

// Social media utilities from https://github.com/kni-labs/rrssb
var popupCenter = function(url, title, w, h) {
  // Fixes dual-screen position                         Most browsers      Firefox
  var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
  var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

  var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
  var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

  var left = ((width / 2) - (w / 2)) + dualScreenLeft;
  var top = ((height / 3) - (h / 3)) + dualScreenTop;

  var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

  // Puts focus on the newWindow
  if (window.focus) {
    newWindow.focus();
  }
};

// IP Callback for locating user's country
function ipcallback(data){
	if(data.address.country_code === 'US'){
		$('body').attr('data-country',data.address.country_code);
	}
}

function closeModal(){
	// Unbind keydown.closeVideo
	$(document).off('keydown.closeModal');

	// Destroy modal
	TweenLite.to('.modal-wrap',.3,{
		opacity: 0,
		onComplete: function(){
			// Destory video
			if($('.modal-wrap').hasClass('video-wrap')){
				var fullVideo = videojs('full-video');
				fullVideo.dispose();
			}

			$('.modal-wrap').remove();
		}
	});

	$('body').removeClass('open-video');
}

$(document).ready(function(){
	var $body = $('body'),
	$header = $('.header');

	// Set js class
	$body.addClass('js').removeClass('no-js');

	// Sprite animation
	function playSpriteAnim(el,loop){
		TweenLite.set(el,{
			backgroundPosition: '0px'
		});
		$animation = TweenMax.to(el,el.dataset.duration,{
			delay: .25,
			backgroundPosition: '-'+el.dataset.width+'px',
			ease:SteppedEase.config(parseInt(el.dataset.frames)),
			repeat: 0,
			onComplete: function(){
				if(el.dataset.loop == 'true'){
					$animation.seek(el.dataset.looptime);
				}
			},
			onStart: function(){
				if(el.dataset.animationonce == 'true'){
					el.dataset.animationonce = 'played';
				}
			}
		});
	}

  // Watch scroll
  function lazyLoad(el,asset,eventType){
  	if(el.dataset.lazyBg){
  		el.style.backgroundImage = 'url('+asset+')';
  	} else {
  		el.setAttribute('src', asset);
  	}

		// Fade element in
		if(el.dataset.animation != 'true' && eventType != 'refresh'){
			TweenMax.fromTo(el,.75,{
				opacity: 0
			},{
				opacity: 1
			});
		}

		// Remove lazy load attribute
		if($(window).width() < 992){
			el.dataset.lazyLoadedMobile = el.dataset.lazyLoadMobile;
			el.removeAttribute('data-lazy-load-mobile');
		} else {
			el.dataset.lazyLoaded = el.dataset.lazyLoad;
			el.removeAttribute('data-lazy-load');
		}
  }
  var sw = new ScrollWatch({
  	watchOnce: false,
  	onElementInView: function(data) {
  		// Trigger animation
  		if(data.el.dataset.animation == 'true' && $(window).width() > 992){
  			if(data.el.dataset.animationonce != 'played'){
  				playSpriteAnim(data.el);
  			}
  		}
  		// Lazy loading
			// Load mobile version
			if($(window).width() < 992){
				if(data.el.dataset.lazyLoadMobile){
					lazyLoad(data.el,data.el.dataset.lazyLoadMobile,data.eventType);
				}
			} else {
	  		if(data.el.dataset.lazyLoad){
	  			// Load desktop version

	  			lazyLoad(data.el,data.el.dataset.lazyLoad,data.eventType);
	  		}
	  	}
  	},
  	onElementOutOfView: function(data){
  		// Reset animation
  		if(data.el.dataset.animation == 'true' && data.el.dataset.animationonce != 'played'){
				TweenLite.set(data.el,{
					backgroundPosition: '0px'
				});
  		}
  	}
  });

  // Set lazy loaded responsive images
  $(window).smartresize(function(){
  	if($(window).width() < 992 && $body.attr('data-lazy-size') != 'mobile'){
  		$('[data-lazy-loaded-mobile]').each(function(){
  			$this = $(this);
  			if($this.attr('data-lazy-bg')){
  				$this.css({'background-image':'url('+$this.attr('data-lazy-loaded-mobile')+')'});
  			} else {
  				$this.attr('src',$this.attr('data-lazy-loaded-mobile'));
  			}
  		});
  		$body.attr('data-lazy-size','mobile');
  	}

  	if($(window).width() > 992 && $body.attr('data-lazy-size') != 'desktop'){
  		$('[data-lazy-loaded]').each(function(){
  			$this = $(this);
  			if($this.attr('data-lazy-bg')){
  				$this.css({'background-image':'url('+$this.attr('data-lazy-loaded')+')'});
  			} else {
  				$this.attr('src',$this.attr('data-lazy-loaded'));
  			}
  		});
  		$body.attr('data-lazy-size','desktop');
  	}
  });

	// Header main menu links
	$('.main-link').on('click',function(e){
		e.preventDefault();
	});
	// Hoverintent for main menu subnav
	mainLinks = document.querySelectorAll('.main-link-wrap');
	for (var i = 0; i < mainLinks.length; i++) {
		hoverintent(mainLinks[i],function(e) {
			$this = $(this);
			// Open menu
			$this.toggleClass('active').siblings('li').removeClass('active');

			// Animate subnav
			$subNav = $this.find('.sub-nav');
		  TweenLite.set($subNav, {height:"auto"})
		  TweenLite.from($subNav, 0.2, {height:0})

			// Animate subnav items
			TweenMax.staggerFromTo($this.find('li'), 0.2, {
				opacity: 0,
				x: '-10px'
			},{
				opacity: 1,
				x: '0'
			},.1);
		}, function() {
			$this = $(this);

			// Animate subnav
			$subNav = $this.find('.sub-nav');
		  TweenLite.to($subNav, 0.2, {height:0})
		}).options({
			interval: 150
		});
	}

	
	// Logo animations
	$logoAnimDelay = .1;

	var $symbolPaths = $('#symbols path'),
			$cityLine1 = $('.city-line-stop-1'),
			$cityLine3 = $('.city-line-stop-3'),
			$uDaytonWord = $('#university-of-dayton'),
		  logoTimeline = new TimelineLite({paused:true});

	logoTimeline
	.addLabel('animIn')
	.to($('.letter-h'),.35,{
		opacity: 1
	},0)
	.to($('.letter-n'),.35,{
		opacity: 1
	},0)

	.to($('.letter-t-1'),.35,{
		opacity: 1
	},0)
	.to($('.letter-t-2'),.35,{
		opacity: 1
	},0)

	.to([$symbolPaths[2]],.35,{
		opacity: 1
	},0)

	.to([$symbolPaths[1],$symbolPaths[3]],.35,{
		opacity: 1
	},$logoAnimDelay*1)
	.to([$symbolPaths[0],$symbolPaths[4]],.35,{
		opacity: 1
	},$logoAnimDelay*2)

	.to($('.letter-c'),.35,{
		opacity: 1
	},$logoAnimDelay*1)
	.to($('.letter-a'),.35,{
		opacity: 1
	},$logoAnimDelay*1)

	.to($('.letter-s'),.35,{
		opacity: 1
	},$logoAnimDelay*1)
	.to($('.letter-u'),.35,{
		opacity: 1
	},$logoAnimDelay*1)

	.to($('.letter-n-2'),.35,{
		opacity: 1
	},$logoAnimDelay*2)
	.to($('.letter-t-3'),.35,{
		opacity: 1
	},$logoAnimDelay*2)

	.to($('.letter-i'),.35,{
		opacity: 1
	},$logoAnimDelay*3)
	.to($('.letter-e'),.35,{
		opacity: 1
	},$logoAnimDelay*3)

	.to($cityLine1,.25,{
		attr: {
			offset: 0
		}
	},$logoAnimDelay*1)

	.to($cityLine3,.25,{
		attr: {
			offset: 1
		}
	},$logoAnimDelay*1)

	.fromTo($uDaytonWord,.35,{
		y: '-20px',
		opacity: 0
	},{
		y: '0px',
		opacity: 1
	},$logoAnimDelay*4)

	// Logo animation out

	.addLabel('fadeOut')
	.addPause('fadeOut')

	.to($('#china-institute path'),.25,{
		opacity: 0
	},'fadeOut')
	.to($symbolPaths,.35,{
		opacity: 0
	},'fadeOut')
	.to($cityLine1,.25,{
		attr: {
			offset: .35
		}
	},'fadeOut')
	.to($cityLine3,.25,{
		attr: {
			offset: .65
		}
	},'fadeOut')

	.to($uDaytonWord,.25,{
		opacity: 0
	},'fadeOut');

	$header.on('mouseenter',function(){

		// Symbol animation
		logoTimeline.delay(.15).play('animIn');

	});

	// Remove active link when user blurs header
	$('.header').on('mouseleave',function(){
		$('.header .active').removeClass('active');

		// Reverse symbol animation
		logoTimeline.play('fadeOut');
	});

	// Mobile header
	// Menu Timeline
	var menuTimeline = new TimelineLite({paused:true});

	// Animate the burger lines
	menuTimeline.to($('.line-one',$header),.4,{
		ease: Cubic.easeOut,
		y: 6,
		rotation: 45
	},0)
	.to($('.line-two',$header),.4,{
		ease: Cubic.easeOut,
		scale: 0,
		opacity: 0
	},0)
	.to($('.line-three',$header),.4,{
		ease: Cubic.easeOut,
		y: -6,
		rotation: -45
	},0);
	$('.btn-menu').on('click',function(e){
		e.preventDefault();

		// Toggle class on body
		$body.toggleClass('open-menu');

		if($body.hasClass('open-menu')){
			// Open menu
			menuTimeline.play();
		} else {
			// Close menu
			mobileMenuClose();
		}
	});

	// Close mobile menu
	function mobileMenuClose(){
		menuTimeline.reverse();
		$body.removeClass('open-menu');
	}

	// Header main menu link highlighting while scrolling
	// Adapted from https://github.com/cmpolis/scrollIt.js

	// Watch scrolling and sets the currently active section
	var updateActive = function(sectionName) {
    $('.header li.active-section').removeClass('active-section');
    $('.header a').removeClass('active-section').filter(function() {
	    return $(this).attr('href') === '#' + sectionName;
	  }).addClass('active-section').closest('.main-link-wrap').addClass('active-section');
	}

	// Watch scrolling for currently active section
	var watchActive = function() {
    var winTop = $(window).scrollTop();
		var $scrollOffset = $(window).height() / 2;

    var visible = $('[data-section]').filter(function(sectionName, div) {
      return winTop >= $(div).offset().top - $scrollOffset &&
        winTop < $(div).offset().top - ($scrollOffset) + $(div).outerHeight()
    });
    var newActive = visible.first().attr('data-section');

    updateActive(newActive);
	}
  
  $(window).on('scroll', watchActive);

  // Header scrolling animation
  $('.main-menu a, .logo').on('click',function(e){
  	e.preventDefault();

  	$href = $(this).attr('href').replace('#','');
  	$target = $('[data-section="' + $href + '"]');

  	$targetTop = $target.offset().top;

  	// Account for nav bar if on mobile
  	if($(window).width() < 992){
  		$targetTop = $targetTop - 50;
  	}

  	TweenLite.to(window, .75, {scrollTo:{y:$targetTop}, ease:Power2.easeOut});

		$('.header .active').removeClass('active');
		
		mobileMenuClose();
  });

	// Share
	$('.btn-share').on('click',function(e){
		e.preventDefault();
		$body.addClass('open-share');

		// Animate share wrap in
		TweenLite.fromTo('.share-wrap',.35,{
			opacity: 0
		},{
			opacity: 1
		})

		// Check country and show China if necessary
		if($body.attr('data-country') == 'CN'){
			$('.social-toggle a[href="#social-china"]').trigger('click');
		} else {
			console.log('usa');
			$('.social-toggle a[href="#social-usa"]').trigger('click');
		}

		// Hide the video when ESC is pressed
		$(document).one('keydown.closeShare',function(e) {
		  $keyCode = e.keyCode;
		  27 == $keyCode && closeShare();
		});
	});

	// Share toggles
	$('.social-toggle a').on('click',function(e){
		e.preventDefault();
		$this = $(this);

		$('.social-toggle a').removeClass('active').filter($this).addClass('active');

		$('.social-grid li').removeClass('active').filter('[data-location="'+ $(this).attr('data-location') +'"]').addClass('active');
	});

	$('.share-wrap .icon-close').on('click',function(e){
		e.preventDefault();
		closeShare();
	});

	// Close share
	function closeShare(){
		$body.removeClass('open-share');

		// Unbind the ESC key to close share
    $(document).off('keydown.closeShare');
	}

	// Carousels
	$('.carousel').each(function(){
		$this = $(this);
		// Get the total slides
		$totalCount = $this.find('.slide-content').length;
		// Append slide count to each slide
		$this.find('.slide-count-wrap').append($totalCount);

		// Init slick
		$this.slick({
			draggable: true,
			fade: true,
			cssEase: 'ease-out',
			slide: '.slide',
			useTransform: true,
			arrows: false
		});
	});

	// Carousel Arrows
	$(document).on('click','.arw-carousel',function(){
		$this = $(this);
		$carousel = $this.closest('.carousel-wrap').find('.carousel');

		if($this.hasClass('prev')){
			$carousel.slick('slickPrev');
		} else {
			$carousel.slick('slickNext');
		}
	});

	// Toggle
	$('.toggle a').on('click',function(e){
		e.preventDefault();
		$this = $(this);

		// Set active class
		$this.closest('li').addClass('active').siblings('li').removeClass('active');

		// Show related content
		$($this.attr('href')).addClass('active').siblings('.toggle-view').removeClass('active');

		// Lazy load images in next tab
		sw.refresh();

		// Animate toggle in
		TweenMax.staggerFromTo($($this.attr('href')).find('.toggle-piece'),.35,{
			y: '20px',
			opacity: 0
		},{
			y: '0px',
			opacity: 1
		},.15);

		// Dropdown toggles
		$dropdown = $this.closest('.dropdown-toggle');
		if($dropdown.length){
			$dropdown.removeClass('open-dropdown').find('.dropdown-selected-copy').text($this.text());
		}
	});

	// Dropdown toggles
	$('.dropdown-toggle').hover(function(e){
		$(this).addClass('open-dropdown');
	},function(){
		$(this).removeClass('open-dropdown');
	});

	// Set first toggle/group to be active
	$('.toggle-view-wrap').each(function(){
		$(this).find('.toggle li:first a').trigger('click');
	});

	// Accordion
	$('.btn-accordion').on('click',function(e){
		e.preventDefault();
		var $this = $(this),
				$accordion = $this.closest('.accordion-wrap').find('.accordion-content');

		if($this.hasClass('open')){
		  TweenLite.to($accordion, 0.2, {height:0})
		  $this.removeClass('open');

		  TweenLite.to($this.find('.vert-line'),0.2,{
		  	ease: Power2.easeOut,
		  	rotation: 0,
		  	transformOrigin: 'center center'
		  });
		}else{
		  TweenLite.set($accordion, {height:"auto"})
		  TweenLite.from($accordion, 0.2, {height:0})
		  $this.addClass('open');

		  TweenLite.to($this.find('.vert-line'),0.2,{
		  	ease: Power2.easeOut,
		  	rotation: 90,
		  	transformOrigin: 'center center'
		  });
		}
	});

	// Gallery modal
	$('.gallery .piece').on('click',function(e, galleryNav){
		$this = $(this);

		// Add gallery modal
		if(!galleryNav){
			$body.append('<div class="modal-wrap gallery-modal"><a href="#" class="icon-svg icon-close"><svg><use xlink:href="/img/spritemap.svg#close"></use></svg></a><div class="container"><div class="row"><div class="col-md-10 col-md-offset-1"><img class="img-full modal-img" src="'+$this.attr('data-img-full')+'"><div class="row"><div class="col-md-8 col-lg-6 modal-copy">'+$this.find('.copy').html()+'</div></div></div></div><span class="icon-svg arw-modal next icon-arw-right"><svg><use xlink:href="/img/spritemap.svg#arw-right"></use></svg></span><span class="icon-svg arw-modal icon-arw-left"><svg><use xlink:href="/img/spritemap.svg#arw-left"></use></svg></span></div></div>').addClass('open-modal');

			// Fade in modal-wrap
			TweenLite.fromTo('.modal-wrap',.3,{
				opacity: 0
			},{
				opacity: 1
			});

			// Hide the modal when ESC is pressed
			$(document).one('keydown.closeModal',function(e) {
			  $keyCode = e.keyCode;
			  27 == $keyCode && closeModal();
			});
		} else {

			// Change modal content
			$('.modal-img').attr('src',$this.attr('data-img-full'));
			$('.modal-copy').html($this.find('.copy').html());

			// Fade in modal content
			TweenMax.staggerFromTo('.modal-wrap img, .modal-copy',.35,{
				y: '20px',
				opacity: 0
			},{
				y: '0px',
				opacity: 1
			},.15);
		}

		// Add active to the gallery piece for modal functionality
		$this.addClass('active').siblings('.piece').removeClass('active');
	});

	// Gallery modal arrows
	$(document).on('click','.modal-wrap .arw-modal',function(){
		if($(this).hasClass('next')){
			$galPiece = $('.gallery .piece.active').next('.piece');
		} else {
			$galPiece = $('.gallery .piece.active').prev('.piece');
		}

		$galPiece.trigger('click',['galleryNav']);
	});

	// Close modal-wrap
	$(document).on('click','.modal-wrap .icon-close',function(e){
		e.preventDefault();

		// Destroy modal
		closeModal();
	});

	// Popup links
	$(document).on('click','.popup-link', function(e){
	  e.preventDefault();
	  var $this = $(this);
	  popupCenter($this.attr('href'), $this.find('.text').html(), 580, 470);
	});



	// Video player functionality
	$('.btn-video').on('click',function(e){
		e.preventDefault();
		// Add video wrap
		$('body').append('<div class="modal-wrap video-wrap"><a href="#" class="icon-svg icon-close"><svg><use xlink:href="/img/spritemap.svg#close"></use></svg></a><video id="full-video" class="video-js vjs-default-skin" controls preload="auto"><source src="'+$(this).attr('data-video')+'" type="video/mp4" /></video></div>').addClass('open-video');

		$modalWrap = $('.modal-wrap');

		TweenLite.fromTo('.video-wrap',.3,{
			opacity: 0
		},{
			opacity: 1
		});

		// Hide the video when ESC is pressed
		$(document).one('keydown.closeModal',function(e) {
		  $keyCode = e.keyCode;
		  27 == $keyCode && closeModal();
		});

		// Player (this) is initialized and ready.
		videojs("full-video", {
			bigPlayButton: false,
			controlBar: {
				currentTimeDisplay: false,
				durationDisplay: false,
				remainingTimeDisplay: false,
				muteToggle: false,
		    volumeControl: false,
		    volumeMenuButton: false,
		    fullscreenToggle: false
			}
		}, function(){
		  var fullVideo = this;
	    fullVideo.play();

	    // Add inactive class when user is inactive
	    fullVideo.on('userinactive',function(){
	    	$modalWrap.addClass('video-inactive');

	    // Remove inactive class when user is active
	    }).on('useractive',function(){
	    	$modalWrap.removeClass('video-inactive');

	    // Close modal when video is over	    	
	    }).on('ended',function(){
	    	closeModal();
	    });
		});
	});

	// Play sound of word
	$('.play-word').on('click',function(){
		$this = $(this);
		// Play sound
		$.playSound($this.attr('data-audio'));

		// Add animating class to wrapper
		if(!$this.hasClass('is-animating')){
			$this.addClass('is-animating');

			// Create timeline
			var $phonetic = new TimelineLite({
				// Reverse the animation after delay
				onComplete: function(){
					TweenMax.delayedCall(1, function(){  
						$phonetic.reverse();
			    });
				},
				// Remove class after reverse
				onReverseComplete: function(){
					$this.removeClass('is-animating');
				}
			});

			// Animate elements
			$phonetic.to($this.closest('.hero').find('.bg-dark'),.5,{
				autoAlpha: true
			},'hero').to($this.find('.heading'),.5,{
				autoAlpha: false
			},'hero').to($this.find('.phonetic'),.5,{
				autoAlpha: true
			},'hero');
		}
	});
});
//# sourceMappingURL=js/maps/scripts.js.map
