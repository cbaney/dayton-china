<div class="header">
	<a href="#home" class="logo">
		<span class="icon-svg icon-logo">
			<svg version="1.1" id="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="229.1px" height="104.4px" viewBox="0 0 229.1 104.4" enable-background="new 0 0 229.1 104.4" xml:space="preserve">
			<g id="university-of-dayton">
				<g>
					<path fill="currentColor" d="M22.9,97.1h1v4.3c0,0.6,0.2,1.1,0.5,1.5c0.3,0.3,0.7,0.5,1.3,0.5c0.6,0,1-0.2,1.3-0.5
						c0.3-0.3,0.5-0.8,0.5-1.4v-4.5h1v4.4c0,0.9-0.2,1.5-0.7,2.1c-0.5,0.5-1.1,0.8-2,0.8c-0.8,0-1.5-0.3-2-0.8
						c-0.5-0.5-0.8-1.3-0.8-2.3V97.1z"/>
					<path fill="currentColor" d="M33,96.8l5.2,5.3v-5h1v7.3L34,99.1v5h-1V96.8z"/>
					<path fill="currentColor" d="M44,97.1h0.9v7H44V97.1z"/>
					<path fill="currentColor" d="M49.3,97.1h1l2.2,5l2.2-5h1l-3.2,7.2L49.3,97.1z"/>
					<path fill="currentColor" d="M59.9,97.1h3.9v1h-2.9v1.7h2.9v0.9h-2.9v2.4h2.9v0.9h-3.9V97.1z"/>
					<path fill="currentColor" d="M68.5,97.1H70c0.8,0,1.4,0.2,1.8,0.6c0.5,0.4,0.7,0.9,0.7,1.4c0,0.5-0.2,0.9-0.5,1.2
						c-0.3,0.4-0.8,0.6-1.4,0.8l2,3h-1.1l-1.9-2.9h-0.2v2.9h-1V97.1z M69.4,97.9v2.4c1.4,0,2.1-0.4,2.1-1.2c0-0.3-0.1-0.5-0.2-0.7
						c-0.2-0.2-0.4-0.3-0.7-0.4C70.4,98,70,97.9,69.4,97.9z"/>
					<path fill="currentColor" d="M81.6,97.9l-0.8,0.6c-0.2-0.2-0.4-0.4-0.6-0.5c-0.2-0.1-0.4-0.2-0.7-0.2c-0.3,0-0.6,0.1-0.8,0.2
						c-0.2,0.2-0.3,0.4-0.3,0.6c0,0.2,0.1,0.4,0.3,0.6c0.2,0.2,0.5,0.3,1,0.5c0.5,0.2,0.8,0.4,1.1,0.5c0.3,0.2,0.5,0.4,0.6,0.6
						c0.2,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5c-0.4,0.4-0.9,0.6-1.5,0.6c-0.6,0-1.1-0.2-1.6-0.5
						c-0.4-0.3-0.8-0.8-1-1.4l1-0.3c0.3,0.8,0.9,1.2,1.6,1.2c0.3,0,0.6-0.1,0.9-0.3c0.2-0.2,0.3-0.5,0.3-0.8c0-0.2-0.1-0.4-0.2-0.6
						s-0.3-0.4-0.5-0.5c-0.2-0.1-0.5-0.3-0.9-0.4c-0.4-0.2-0.7-0.3-0.9-0.4c-0.2-0.1-0.4-0.3-0.5-0.5c-0.2-0.2-0.3-0.4-0.3-0.6
						c-0.1-0.2-0.1-0.4-0.1-0.6c0-0.5,0.2-0.9,0.6-1.3c0.4-0.4,0.9-0.5,1.5-0.5c0.4,0,0.8,0.1,1.2,0.2C81,97.3,81.4,97.5,81.6,97.9z"/>
					<path fill="currentColor" d="M86.4,97.1h0.9v7h-0.9V97.1z"/>
					<path fill="currentColor" d="M91.8,97.1h4.3v1h-1.7v6.1h-0.9V98h-1.7V97.1z"/>
					<path fill="currentColor" d="M100.1,97.1h1.1l1.7,2.8l1.7-2.8h1.1l-2.3,3.8v3.2h-1v-3.2L100.1,97.1z"/>
					<path fill="currentColor" d="M117.5,100.5c0-1,0.4-1.9,1.1-2.6c0.8-0.7,1.7-1.1,2.8-1.1c1,0,1.9,0.4,2.7,1.1c0.8,0.7,1.1,1.6,1.1,2.6
						c0,1.1-0.4,2-1.1,2.7s-1.7,1.1-2.7,1.1c-1.1,0-2-0.4-2.7-1.1C117.8,102.5,117.5,101.6,117.5,100.5z M124.2,100.6
						c0-0.8-0.3-1.4-0.8-2c-0.6-0.5-1.2-0.8-2.1-0.8c-0.8,0-1.4,0.3-2,0.8c-0.6,0.5-0.8,1.2-0.8,1.9c0,0.8,0.3,1.4,0.9,1.9
						c0.6,0.5,1.3,0.8,2.1,0.8c0.8,0,1.4-0.3,2-0.8C123.9,102,124.2,101.3,124.2,100.6z"/>
					<path fill="currentColor" d="M129.7,97.1h3.9v1h-2.9v1.7h2.9v0.9h-2.9v3.4h-1V97.1z"/>
					<path fill="currentColor" d="M145.7,97.1h1.7c1.3,0,2.3,0.4,3,1.1c0.7,0.7,1,1.5,1,2.4c0,0.9-0.3,1.7-1,2.4c-0.7,0.7-1.7,1.1-3,1.1
						h-1.7V97.1z M146.7,98v5.1h0.1c0.7,0,1.2,0,1.6-0.1c0.4-0.1,0.7-0.2,1-0.4c0.3-0.2,0.6-0.5,0.8-0.9c0.2-0.4,0.3-0.8,0.3-1.2
						c0-0.5-0.2-1-0.5-1.5c-0.3-0.4-0.7-0.7-1.1-0.9c-0.4-0.2-1-0.2-1.7-0.2H146.7z"/>
					<path fill="currentColor" d="M158.3,96.8l3.3,7.3h-1l-0.8-1.8h-3l-0.8,1.8h-1L158.3,96.8z M158.3,99l-1.1,2.5h2.3L158.3,99z"/>
					<path fill="currentColor" d="M164.8,97.1h1.1l1.7,2.8l1.7-2.8h1.1l-2.3,3.8v3.2h-1v-3.2L164.8,97.1z"/>
					<path fill="currentColor" d="M174.4,97.1h4.3v1H177v6.1H176V98h-1.7V97.1z"/>
					<path fill="currentColor" d="M182.6,100.5c0-1,0.4-1.9,1.1-2.6c0.8-0.7,1.7-1.1,2.8-1.1c1,0,1.9,0.4,2.7,1.1c0.8,0.7,1.1,1.6,1.1,2.6
						c0,1.1-0.4,2-1.1,2.7s-1.7,1.1-2.7,1.1c-1.1,0-2-0.4-2.7-1.1C183,102.5,182.6,101.6,182.6,100.5z M189.3,100.6
						c0-0.8-0.3-1.4-0.8-2c-0.6-0.5-1.2-0.8-2.1-0.8c-0.8,0-1.4,0.3-2,0.8c-0.6,0.5-0.8,1.2-0.8,1.9c0,0.8,0.3,1.4,0.9,1.9
						c0.6,0.5,1.3,0.8,2.1,0.8c0.8,0,1.4-0.3,2-0.8C189,102,189.3,101.3,189.3,100.6z"/>
					<path fill="currentColor" d="M194.9,96.8l5.2,5.3v-5h1v7.3l-5.2-5.3v5h-1V96.8z"/>
				</g>
			</g>
			<g id="symbols">
				<g>
					<path fill="currentColor" d="M54.1,11.4c1.5-0.5,3-1,4.5-1.5c0.5,1.7,1.1,3.5,1.6,5.2c-0.3,0.1-0.5,0.2-0.8,0.3
						c-0.1-0.2-0.1-0.4-0.2-0.7c-1.2,0.4-2.4,0.8-3.5,1.2c0.4,1.2,0.8,2.4,1.3,3.7c-0.3,0.1-0.5,0.2-0.8,0.3c-0.4-1.2-0.9-2.4-1.3-3.7
						c-1.2,0.4-2.3,0.8-3.5,1.3c0.1,0.2,0.2,0.5,0.3,0.7c-0.3,0.1-0.5,0.2-0.8,0.3c-0.7-1.7-1.3-3.5-2-5.2c1.4-0.5,2.9-1.1,4.4-1.6
						c-0.2-0.7-0.5-1.3-0.7-2c0.4-0.1,0.7-0.2,1.1-0.3c0,0.1,0,0.2-0.2,0.2C53.7,10.2,53.9,10.8,54.1,11.4z M51.1,16.8
						c1.2-0.4,2.3-0.9,3.5-1.3c-0.4-1-0.7-2-1.1-3.1c-1.2,0.4-2.4,0.8-3.6,1.3C50.3,14.7,50.7,15.7,51.1,16.8z M55.4,15.2
						c1.2-0.4,2.4-0.8,3.6-1.2c-0.3-1-0.7-2.1-1-3.1c-1.2,0.4-2.4,0.8-3.6,1.2C54.7,13.2,55,14.2,55.4,15.2z"/>
					<path fill="currentColor" d="M77.5,3.5c3.5-0.7,6.9-1.3,10.4-1.8c0.5,3.4,0.9,6.8,1.4,10.2c-0.3,0-0.5,0.1-0.8,0.1
						c0-0.2-0.1-0.4-0.1-0.6c-2.8,0.4-5.5,0.9-8.3,1.4c0,0.2,0.1,0.4,0.1,0.6c-0.3,0-0.5,0.1-0.8,0.2C78.8,10.2,78.2,6.8,77.5,3.5z
						 M80,12.1c2.8-0.5,5.5-1,8.3-1.4C88,8,87.6,5.3,87.2,2.6c-2.9,0.4-5.8,0.9-8.7,1.5C79,6.8,79.5,9.4,80,12.1z M84.2,9.5
						c1-0.2,2.1-0.3,3.1-0.5c0,0.2,0.1,0.5,0.1,0.7c-2.2,0.3-4.5,0.7-6.7,1.1c0-0.2-0.1-0.4-0.1-0.7c1-0.2,1.9-0.4,2.9-0.5
						c-0.1-0.7-0.2-1.4-0.4-2.1c-0.8,0.1-1.6,0.3-2.4,0.4c0-0.2-0.1-0.4-0.1-0.7C81.4,7.2,82.2,7,83,6.9c-0.1-0.6-0.2-1.2-0.3-1.8
						c-0.9,0.2-1.8,0.3-2.7,0.5c0-0.2-0.1-0.4-0.1-0.7c2.1-0.4,4.3-0.8,6.4-1.1c0,0.2,0.1,0.4,0.1,0.7c-1,0.1-1.9,0.3-2.9,0.5
						c0.1,0.6,0.2,1.2,0.3,1.8c0.9-0.1,1.7-0.3,2.6-0.4c0,0.2,0.1,0.4,0.1,0.7c-0.9,0.1-1.7,0.3-2.6,0.4C84,8.1,84.1,8.8,84.2,9.5z
						 M86.2,9c-0.3-0.3-0.9-0.8-1.4-1.1c0.2-0.1,0.3-0.2,0.5-0.4c0.5,0.3,1.2,0.8,1.5,1.1C86.6,8.7,86.4,8.8,86.2,9z"/>
					<path fill="currentColor" d="M111.6,8.6c-0.8,0-1.7,0-2.5,0c0,0.3,0,0.7,0,1c-0.2,0-0.5,0-0.7,0c0-1.3-0.1-2.7-0.1-4
						c-0.2,0.4-0.4,0.8-0.7,1.2c-0.1-0.2-0.3-0.5-0.5-0.6c0.8-1.2,1.4-3.3,1.6-5.4c-0.5,0-1.1,0-1.6,0c0-0.2,0-0.5,0-0.7
						c1.6-0.1,3.2-0.1,4.9-0.1c0,0.2,0,0.5,0,0.7c-0.8,0-1.6,0-2.4,0c-0.1,1-0.3,1.9-0.6,2.8c0.9,0,1.7,0,2.6,0
						C111.5,5.2,111.6,6.9,111.6,8.6z M110.8,7.9c0-1.2,0-2.4,0-3.6c-0.6,0-1.2,0-1.8,0c0,1.2,0.1,2.4,0.1,3.6
						C109.6,7.9,110.2,7.9,110.8,7.9z M118.5,4.5c0,0.2,0,0.5,0,0.7c-0.5,0-1,0-1.5,0c0,1.7-0.1,3.4-0.1,5.1c-0.3,0-0.5,0-0.8,0
						c0-1.7,0.1-3.4,0.1-5.1c-0.6,0-1.3,0-1.9,0c-0.1,1.9-0.6,3.7-2.4,5.1c-0.1-0.1-0.4-0.3-0.6-0.4c1.7-1.3,2.1-3,2.2-4.7
						c-0.5,0-1,0-1.5,0c0-0.2,0-0.5,0-0.7c0.5,0,1,0,1.6,0c0-1.2,0-2.4,0-3.6c-0.4,0-0.8,0-1.2,0c0-0.2,0-0.5,0-0.7c2,0,4.1,0,6.1,0.1
						c0,0.2,0,0.5,0,0.7c-0.4,0-0.9,0-1.3,0c0,1.2-0.1,2.4-0.1,3.6C117.5,4.4,118,4.4,118.5,4.5z M114.3,4.4c0.6,0,1.3,0,1.9,0
						c0-1.2,0-2.4,0.1-3.6c-0.7,0-1.3,0-2,0C114.3,1.9,114.3,3.2,114.3,4.4z"/>
					<path fill="currentColor" d="M144.3,11.9c-0.1,0.3,0,0.4,0.3,0.4c0.3,0.1,0.7,0.1,1,0.2c0.3,0.1,0.4-0.1,0.7-1.6
						c0.2,0.2,0.5,0.4,0.7,0.5c-0.4,1.6-0.7,2-1.5,1.8c-0.4-0.1-0.8-0.2-1.2-0.2c-0.8-0.2-1-0.4-0.9-1.2c0.1-0.8,0.3-1.7,0.4-2.5
						c-0.9-0.2-1.8-0.3-2.6-0.4c-0.8,1.7-2,2.8-4.9,3c0-0.2-0.2-0.5-0.4-0.7c2.5-0.2,3.7-1.1,4.4-2.5c-1.1-0.2-2.2-0.3-3.2-0.5
						c0-0.2,0.1-0.5,0.1-0.7c1.1,0.2,2.3,0.3,3.4,0.5c0.2-0.5,0.3-1,0.5-1.6c0.8,0.2,0.9,0.2,1.1,0.3c0,0.1-0.1,0.1-0.3,0.1
						c-0.1,0.5-0.3,0.9-0.4,1.3c1.1,0.2,2.2,0.4,3.3,0.6C144.7,9.7,144.5,10.8,144.3,11.9z M141.6,4c0,0.1-0.1,0.1-0.3,0.1
						c-0.6,1.4-1.5,2.4-4.2,2.6c0-0.2-0.2-0.5-0.4-0.7c2.4-0.1,3.3-0.9,3.7-2.3C140.9,3.8,141.2,3.9,141.6,4z M138.4,3.2
						c-0.1,0.5-0.1,0.9-0.2,1.4c-0.3,0-0.5-0.1-0.8-0.1c0.1-0.7,0.2-1.4,0.3-2.1c1.6,0.2,3.2,0.5,4.8,0.7c0.1-0.4,0.1-0.9,0.2-1.3
						c0.4,0.1,0.7,0.2,1.1,0.2c0,0.1-0.1,0.1-0.3,0.1c-0.1,0.4-0.1,0.7-0.2,1.1c1.6,0.3,3.2,0.6,4.8,0.9c-0.1,0.7-0.3,1.4-0.4,2.1
						c-0.3-0.1-0.6-0.1-0.8-0.2c0.1-0.5,0.2-0.9,0.3-1.4C144.2,4.1,141.3,3.6,138.4,3.2z M143.7,4.1c0.3,0.1,0.7,0.2,1,0.3
						c0,0.1-0.1,0.1-0.3,0.1c-0.1,0.7-0.2,1.3-0.3,2c0,0.3,0,0.3,0.5,0.4c0.6,0.1,1.2,0.2,1.8,0.3c0.4,0.1,0.4,0,0.6-0.6
						c0.1,0.1,0.5,0.3,0.7,0.4c-0.3,0.8-0.5,1-1.3,0.9c-0.6-0.1-1.3-0.2-1.9-0.4c-1.1-0.2-1.3-0.4-1.2-1.2
						C143.5,5.6,143.6,4.9,143.7,4.1z"/>
					<path fill="currentColor" d="M170.4,9.1c0.1,0.2,0.3,0.3,0.4,0.5c0,0-0.1,0.1-0.1,0.1c-0.6,0.7-1.4,1.7-2.1,2.5
						c0.6,1.2,0.7,2.1,0.4,2.8c-0.5,1.6-1.5,1.2-2.4,0.9c0-0.2,0.1-0.5,0-0.7c0.2,0.1,0.3,0.1,0.5,0.2c0.5,0.1,0.9,0.3,1.2-0.6
						c0.2-0.6,0.1-1.5-0.5-2.7c0.6-0.7,1.3-1.6,1.9-2.3c-0.6-0.2-1.1-0.4-1.7-0.5c-1,3.1-1.9,6.1-2.9,9.2c-0.2-0.1-0.5-0.1-0.7-0.2
						c1-3.3,2-6.6,3-9.8c0.9,0.3,1.9,0.6,2.8,0.9C170.3,9,170.3,9,170.4,9.1z M172.7,19.4c-0.1,0.3-0.1,0.3,0.2,0.4
						c0.3,0.1,0.6,0.2,0.8,0.3c0.3,0.1,0.4-0.1,0.8-1.3c0.1,0.2,0.4,0.4,0.6,0.5c-0.6,1.4-0.9,1.7-1.6,1.4c-0.3-0.1-0.7-0.3-1-0.4
						c-0.7-0.3-0.9-0.5-0.6-1.3c0.4-1,0.7-2,1.1-3c-0.5-0.2-1-0.3-1.5-0.5c-0.9,2-1.8,3.3-4.2,3.3c0-0.2-0.1-0.5-0.2-0.7
						c2.1,0,3-1,3.7-2.8c-0.5-0.2-1.1-0.4-1.6-0.5c0.1-0.2,0.2-0.5,0.2-0.7c2.2,0.7,4.5,1.5,6.7,2.4c-0.1,0.2-0.2,0.4-0.3,0.7
						c-0.7-0.3-1.4-0.5-2.1-0.8C173.4,17.4,173,18.4,172.7,19.4z M171.2,11c-0.2,0.5-0.3,0.9-0.5,1.4c-0.2-0.1-0.5-0.2-0.7-0.2
						c0.2-0.7,0.4-1.4,0.7-2c1,0.3,2,0.7,3,1c0.2-0.4,0.3-0.9,0.5-1.3c0.3,0.1,0.7,0.3,1,0.4c0,0.1-0.1,0.1-0.3,0.1
						c-0.1,0.4-0.3,0.8-0.4,1.1c1,0.4,2,0.7,3.1,1.1c-0.3,0.7-0.5,1.3-0.8,2c-0.2-0.1-0.5-0.2-0.7-0.3c0.2-0.5,0.3-0.9,0.5-1.4
						C174.8,12.3,173,11.6,171.2,11z M175.6,14.8c-1.6-0.6-3.3-1.2-4.9-1.7c0.1-0.2,0.1-0.4,0.2-0.7c1.6,0.6,3.3,1.1,4.9,1.7
						C175.8,14.3,175.7,14.6,175.6,14.8z"/>
				</g>
			</g>
			<g id="city">
				<g>
					<g>
						<path fill="currentColor" d="M112.2,32h1.7v25.5h-1.7V32z"/>
					</g>
					<g>
						<path fill="currentColor" d="M108,42.8h1.7v14.7H108V42.8z"/>
					</g>
					<g>
						<path fill="currentColor" d="M120.6,44.9h1.7v12.5h-1.7V44.9z"/>
					</g>
					<g>
						<path fill="currentColor" d="M103.8,48.4h1.7v9.1h-1.7V48.4z"/>
					</g>
					<g>
						<path fill="currentColor" d="M116.4,49.4h1.7v8.1h-1.7V49.4z"/>
					</g>
				</g>
				<g id="XMLID_14_" opacity="0.5">
					<g id="XMLID_37_">
						<linearGradient class="logo-gradient" id="XMLID_24_" gradientUnits="userSpaceOnUse" x1="113.0764" y1="57.8228" x2="113.0764" y2="86.7616">
							<stop  offset="0" stop-color="currentColor"/>
							<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
						</linearGradient>
						<path id="XMLID_38_" fill="url(#XMLID_24_)" d="M112.2,57.4h1.7v29.8h-1.7V57.4z"/>
					</g>
					<g id="XMLID_34_">
						<linearGradient class="logo-gradient" id="XMLID_25_" gradientUnits="userSpaceOnUse" x1="113.0764" y1="57.5482" x2="113.0764" y2="87.1899">
							<stop  offset="0" stop-color="currentColor"/>
							<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
						</linearGradient>
						<path id="XMLID_35_" fill="url(#XMLID_25_)" d="M112.2,57.4h1.7v29.8h-1.7V57.4z"/>
					</g>
				</g>
				<g opacity="0.5">
					<linearGradient class="logo-gradient" id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="108.8972" y1="57.4321" x2="108.8972" y2="76.202">
						<stop  offset="0" stop-color="currentColor"/>
						<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
					</linearGradient>
					<path fill="url(#SVGID_1_)" d="M108,57.4h1.7v19H108V57.4z"/>
				</g>
				<g opacity="0.5">
					<linearGradient class="logo-gradient" id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="121.4348" y1="57.7274" x2="121.4348" y2="74.4289">
						<stop  offset="0" stop-color="currentColor"/>
						<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
					</linearGradient>
					<path fill="url(#SVGID_2_)" d="M120.6,57.4h1.7v16.8h-1.7V57.4z"/>
				</g>
				<g opacity="0.5">
					<linearGradient class="logo-gradient" id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="104.718" y1="57.6079" x2="104.718" y2="70.6253">
						<stop  offset="0" stop-color="currentColor"/>
						<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
					</linearGradient>
					<path fill="url(#SVGID_3_)" d="M103.8,57.4h1.7v13.4h-1.7V57.4z"/>
				</g>
				<g opacity="0.5">
					<linearGradient class="logo-gradient" id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="117.2556" y1="57.3366" x2="117.2556" y2="69.6566">
						<stop  offset="0" stop-color="currentColor"/>
						<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
					</linearGradient>
					<path fill="url(#SVGID_4_)" d="M116.4,57.4h1.7v12.3h-1.7V57.4z"/>
				</g>
				<linearGradient class="logo-gradient" id="XMLID_26_" gradientUnits="userSpaceOnUse" x1="100.5336" y1="57.5025" x2="100.5336" y2="62.6061">
					<stop  offset="0" stop-color="currentColor"/>
					<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
				</linearGradient>
				<path id="XMLID_15_" fill="url(#XMLID_26_)" d="M99.7,57.4h1.7v5.3h-1.7V57.4z"/>
				<linearGradient class="logo-gradient" id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="125.6089" y1="57.5025" x2="125.6089" y2="62.6061">
					<stop  offset="0" stop-color="currentColor"/>
					<stop  offset="1" stop-color="currentColor" style="stop-opacity:0"/>
				</linearGradient>
				<path opacity="0.5" fill="url(#SVGID_5_)" d="M124.7,57.4h1.7v5.3h-1.7V57.4z"/>
				<g>
					<path fill="currentColor" d="M99.7,52.7h1.7v4.8h-1.7V52.7z"/>
				</g>
				<g>
					<path fill="currentColor" d="M124.7,52.7h1.7v4.8h-1.7V52.7z"/>
				</g>
				<linearGradient class="logo-gradient" id="city-line" gradientUnits="userSpaceOnUse" x1="13.7251" y1="57.4322" x2="211.9649" y2="57.4322">
					<stop class="city-line-stop-1" offset=".35" stop-color="currentColor" style="stop-opacity:0"/>
					<stop  offset="0.5" stop-color="currentColor" style=""/>
					<stop class="city-line-stop-3" offset=".65" stop-color="currentColor" style="stop-opacity:0"/>
				</linearGradient>
				<path fill="url(#city-line)" d="M13.7,57.4c0,0,12.4-0.1,31-0.2c9.3,0,20.1-0.1,31.7-0.1c11.6,0,24,0,36.4,0c12.4,0,24.8,0,36.4,0
					c11.6,0,22.5,0.1,31.7,0.1c18.6,0.1,31,0.2,31,0.2s-12.4,0.1-31,0.2c-9.3,0-20.1,0.1-31.7,0.1c-11.6,0-24,0-36.4,0
					c-12.4,0-24.8,0-36.4,0c-11.6,0-22.5-0.1-31.7-0.1C26.1,57.5,13.7,57.4,13.7,57.4z"/>
			</g>
			<g id="china-institute">
				<path class="letter-c" fill="currentColor" d="M62.2,36.7v2c-1.4-0.7-2.7-1-3.8-1c-1.5,0-2.8,0.5-3.8,1.4c-1,1-1.5,2.2-1.5,3.6c0,1.4,0.5,2.6,1.6,3.6
					c1,1,2.4,1.4,3.9,1.4c1.1,0,2.4-0.4,3.6-1.1v2c-1.2,0.6-2.4,0.9-3.8,0.9c-2,0-3.7-0.7-5.1-2c-1.4-1.4-2.1-3-2.1-4.9
					c0-1.9,0.7-3.5,2.1-4.8c1.4-1.3,3.1-2,5-2C59.5,35.9,60.8,36.2,62.2,36.7z"/>
				<path class="letter-h" fill="currentColor" d="M80.3,36.3H82v5h6.2v-5h1.7v12.9h-1.7V43H82v6.2h-1.8V36.3z"/>
				<path class="letter-n" fill="currentColor" d="M135.3,35.8l9.5,9.8v-9.3h1.8v13.5l-9.5-9.7v9.2h-1.8V35.8z"/>
				<path class="letter-a" fill="currentColor" d="M169,35.8l6.1,13.4h-1.8l-1.5-3.3h-5.5l-1.5,3.3H163L169,35.8z M169,39.8l-2.1,4.6h4.1L169,39.8z"/>


				<path class="letter-i" fill="currentColor" d="M0,65.9h1.7v12.9H0V65.9z"/>
				<path class="letter-n-2" fill="currentColor" d="M23.3,65.3l9.5,9.8v-9.3h1.8v13.5l-9.5-9.7v9.2h-1.8V65.3z"/>
				<path class="letter-s" fill="currentColor" d="M60.9,67.3l-1.4,1.1c-0.4-0.4-0.7-0.7-1-0.9c-0.3-0.2-0.7-0.3-1.3-0.3c-0.6,0-1.1,0.1-1.5,0.4 C55.2,68,55,68.4,55,68.8c0,0.4,0.2,0.7,0.5,1c0.3,0.3,0.9,0.6,1.8,1c0.8,0.4,1.5,0.7,1.9,1c0.5,0.3,0.8,0.6,1.1,1 c0.3,0.4,0.5,0.8,0.7,1.2c0.1,0.4,0.2,0.8,0.2,1.3c0,1.1-0.4,2-1.2,2.7c-0.8,0.8-1.7,1.1-2.8,1.1c-1.1,0-2-0.3-2.9-0.9 c-0.8-0.6-1.5-1.5-1.9-2.7l1.8-0.5c0.6,1.4,1.6,2.2,2.9,2.2c0.6,0,1.2-0.2,1.6-0.6c0.4-0.4,0.6-0.9,0.6-1.5c0-0.3-0.1-0.7-0.3-1.1 c-0.2-0.4-0.5-0.6-0.8-0.9c-0.3-0.2-0.9-0.5-1.6-0.8c-0.7-0.3-1.3-0.6-1.7-0.8c-0.4-0.2-0.7-0.5-1-0.9c-0.3-0.3-0.5-0.7-0.6-1 c-0.1-0.3-0.2-0.7-0.2-1.1c0-0.9,0.4-1.7,1.1-2.4c0.7-0.6,1.6-1,2.7-1c0.7,0,1.4,0.2,2.1,0.5C59.8,66.2,60.4,66.7,60.9,67.3z"/>
				<path class="letter-t-1" fill="currentColor" d="M81.1,65.9H89v1.8h-3.1v11.1h-1.7V67.6h-3.1V65.9z"/>
				<path class="letter-t-2" fill="currentColor" d="M137.3,65.9h7.9v1.8h-3.1v11.1h-1.7V67.6h-3.1V65.9z"/>
				<path class="letter-u" fill="currentColor" d="M164.2,65.9h1.8v7.9c0,1.2,0.3,2.1,0.8,2.7c0.6,0.6,1.4,1,2.4,1c1,0,1.8-0.3,2.4-0.9 c0.6-0.6,0.9-1.4,0.9-2.5v-8.2h1.8V74c0,1.6-0.5,2.8-1.3,3.8c-0.9,0.9-2.1,1.4-3.6,1.4c-1.5,0-2.8-0.5-3.7-1.4 c-0.9-1-1.4-2.4-1.4-4.2V65.9z"/>
				<path class="letter-t-3" fill="currentColor" d="M193.4,65.9h7.9v1.8h-3.1v11.1h-1.7V67.6h-3.1V65.9z"/>
				<path class="letter-e" fill="currentColor" d="M222,65.9h7.1v1.8h-5.4v3.2h5.4v1.7h-5.4V77h5.4v1.7H222V65.9z"/>
			</g>
			</svg>

			<svg version="1.1" id="logo-mobile" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="124px" height="39.2px" viewBox="0 0 124 39.2" enable-background="new 0 0 124 39.2" xml:space="preserve">
				<g>
					<g>
						<g>
							<path fill="#FFFFFF" d="M48.3,8.9v1.3c-0.9-0.5-1.8-0.7-2.6-0.7c-1,0-1.9,0.3-2.5,1c-0.7,0.6-1,1.4-1,2.4c0,1,0.4,1.8,1.1,2.4
								c0.7,0.6,1.6,1,2.6,1c0.8,0,1.6-0.2,2.4-0.7v1.4c-0.8,0.4-1.6,0.6-2.5,0.6c-1.3,0-2.5-0.5-3.4-1.4c-1-0.9-1.4-2-1.4-3.3
								c0-1.3,0.5-2.3,1.4-3.2c0.9-0.9,2.1-1.3,3.4-1.3C46.5,8.3,47.4,8.5,48.3,8.9z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M61.7,8.6h1.2v3.3H67V8.6h1.2v8.6H67v-4.2h-4.1v4.2h-1.2V8.6z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M95.8,8.2l6.4,6.6V8.6h1.2v9l-6.4-6.5v6.1h-1.2V8.2z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M120,8.2l4.1,9h-1.2l-1-2.2h-3.7l-1,2.2h-1.2L120,8.2z M120,10.9l-1.4,3.1h2.8L120,10.9z"/>
						</g>
						<rect x="81.9" y="8.6" fill="#FFFFFF" width="1.2" height="8.6"/>
						<g>
							<g>
								<path fill="#FFFFFF" d="M64.1,22.8l-0.5,0.4c-0.1-0.1-0.3-0.2-0.4-0.3c-0.1-0.1-0.3-0.1-0.5-0.1c-0.2,0-0.4,0.1-0.6,0.2
									c-0.1,0.1-0.2,0.2-0.2,0.4c0,0.1,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.6,0.4c0.3,0.1,0.5,0.2,0.7,0.4c0.2,0.1,0.3,0.2,0.4,0.4
									c0.1,0.1,0.2,0.3,0.2,0.4c0.1,0.1,0.1,0.3,0.1,0.5c0,0.4-0.1,0.7-0.4,1c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.7-0.1-1-0.3
									c-0.3-0.2-0.5-0.5-0.7-1l0.6-0.2c0.2,0.5,0.6,0.8,1.1,0.8c0.2,0,0.4-0.1,0.6-0.2c0.2-0.1,0.2-0.3,0.2-0.5c0-0.1,0-0.3-0.1-0.4
									c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.3-0.2-0.6-0.3c-0.3-0.1-0.5-0.2-0.6-0.3c-0.1-0.1-0.3-0.2-0.4-0.3
									c-0.1-0.1-0.2-0.2-0.2-0.4c0-0.1-0.1-0.3-0.1-0.4c0-0.3,0.1-0.6,0.4-0.9c0.3-0.2,0.6-0.4,1-0.4c0.3,0,0.5,0.1,0.8,0.2
									C63.7,22.4,64,22.6,64.1,22.8z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M49.8,22.1l3.5,3.6v-3.4h0.6v4.9l-3.5-3.5V27h-0.6V22.1z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M40.9,22.5h0.6v4.7h-0.6V22.5z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M90.7,22.3h2.9v0.6h-1.1v4h-0.6v-4h-1.1V22.3z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M111.1,22.3h2.9v0.6h-1.1v4h-0.6v-4h-1.1V22.3z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M121.4,22.3h2.6v0.6h-2v1.2h2v0.6h-2v1.6h2V27h-2.6V22.3z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M100.5,22.3h0.6v2.9c0,0.4,0.1,0.8,0.3,1c0.2,0.2,0.5,0.3,0.9,0.3c0.4,0,0.7-0.1,0.9-0.3
									c0.2-0.2,0.3-0.5,0.3-0.9v-3h0.6v2.9c0,0.6-0.2,1-0.5,1.4c-0.3,0.3-0.8,0.5-1.3,0.5c-0.6,0-1-0.2-1.3-0.5
									c-0.3-0.3-0.5-0.9-0.5-1.5V22.3z"/>
							</g>
						</g>
						<g>
							<g>
								<path fill="#FFFFFF" d="M71.1,22.3H74v0.6h-1.1v4h-0.6v-4h-1.1V22.3z"/>
							</g>
						</g>
						<rect x="82.1" y="22.3" fill="#FFFFFF" width="0.6" height="4.7"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.1908" y1="18.6394" x2="17.1908" y2="39.137">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_1_)" d="M16.6,18.6h1.3v20.6h-1.3V18.6z"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="14.143" y1="18.5591" x2="14.143" y2="31.1333">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_2_)" d="M13.5,18.6h1.3v12.7h-1.3V18.6z"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="23.2865" y1="18.7547" x2="23.2865" y2="29.8182">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_3_)" d="M22.7,18.6h1.3v11.2h-1.3V18.6z"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="11.0951" y1="18.6725" x2="11.0951" y2="27.0728">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_4_)" d="M10.5,18.6h1.3v8.6h-1.3V18.6z"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="8.0463" y1="18.6094" x2="8.0463" y2="22.3314">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_5_)" d="M7.4,18.6h1.3v3.8H7.4V18.6z"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="26.3336" y1="18.6094" x2="26.3336" y2="22.3314">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_6_)" d="M25.7,18.6H27v3.8h-1.3V18.6z"/>
					</g>
					<g opacity="0.5">
						<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="20.2387" y1="18.4982" x2="20.2387" y2="26.3597">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
						</linearGradient>
						<path fill="url(#SVGID_7_)" d="M19.6,18.6h1.3v7.9h-1.3V18.6z"/>
					</g>
					<g>
						<g>
							<path fill="#FFFFFF" d="M16.6,0h1.3v18.6h-1.3V0z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M13.5,7.9h1.3v10.7h-1.3V7.9z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M22.7,9.5h1.3v9.1h-1.3V9.5z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M10.5,12h1.3v6.6h-1.3V12z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M7.4,15.1h1.3v3.5H7.4V15.1z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M25.7,15.1H27v3.5h-1.3V15.1z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M19.6,12.7h1.3v5.9h-1.3V12.7z"/>
						</g>
					</g>
					<linearGradient id="SVGID_8_" gradientUnits="userSpaceOnUse" x1="0" y1="18.6211" x2="34.332" y2="18.6211">
						<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0"/>
						<stop  offset="4.481354e-02" style="stop-color:#FFFFFF;stop-opacity:8.962709e-02"/>
						<stop  offset="0.5" style="stop-color:#FFFFFF"/>
						<stop  offset="0.9552" style="stop-color:#FFFFFF;stop-opacity:8.962709e-02"/>
						<stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
					</linearGradient>
					<path fill="url(#SVGID_8_)" d="M0,18.6c0,0,0.5,0,1.5,0c0.9,0,2.3-0.1,3.9-0.1c1.6,0,3.5-0.1,5.5-0.1c2,0,4.2,0,6.3,0
						c2.1,0,4.3,0,6.3,0c2,0,3.9,0,5.5,0.1c1.6,0,3,0.1,3.9,0.1c0.9,0,1.5,0,1.5,0s-0.5,0-1.5,0c-0.9,0-2.3,0.1-3.9,0.1
						c-1.6,0-3.5,0.1-5.5,0.1c-2,0-4.2,0-6.3,0c-2.1,0-4.3,0-6.3,0c-2,0-3.9,0-5.5-0.1c-1.6,0-3-0.1-3.9-0.1C0.5,18.6,0,18.6,0,18.6z"/>
				</g>
			</svg>
		</span>
		<span class="visuallyhidden">University of Dayton China Institute</span>
	</a>
	<a href="#" class="btn-menu">
		<span class="line-one"></span>
		<span class="line-two"></span>
		<span class="line-three"></span>
		<span class="visuallyhidden">Menu</span>
	</a>
	<div class="menu-wrap">
		<div class="main-menu">
			<ul>
				<li class="main-link-wrap">
					<a href="#explore" class="main-link">
						<span class="icon-svg icon-explore">
							<svg><use xlink:href="/img/spritemap.svg#explore"></use></svg>
						</span>
						Explore
					</a>
					<ul class="sub-nav">
						<li><a href="#suzhou">Suzhou</a></li>
						<li><a href="#campus">Campus</a></li>
						<li><a href="#greater-china">Greater China</a></li>
					</ul>
				</li>
				<li class="main-link-wrap">
					<a href="#meet" class="main-link">
						<span class="icon-svg icon-meet">
							<svg><use xlink:href="/img/spritemap.svg#meet"></use></svg>
						</span>
						meet
					</a>
					<ul class="sub-nav">
						<li><a href="#our-students">Our Students</a></li>
						<li><a href="#our-faculty-members">Our Faculty Members</a></li>
						<li><a href="#the-people-of-suzhou">The People of Suzhou</a></li>
					</ul>
				</li>
				<li class="main-link-wrap">
					<a href="#expand" class="main-link">
						<span class="icon-svg icon-expand">
							<svg><use xlink:href="/img/spritemap.svg#expand"></use></svg>
						</span>
						expand
					</a>
					<ul class="sub-nav">
						<li><a href="#academics">Academics</a></li>
						<li><a href="#university-partnerships">University partnerships</a></li>
					</ul>
				</li>
				<li class="main-link-wrap">
					<a href="#immerse" class="main-link">
						<span class="icon-svg icon-immerse">
							<svg><use xlink:href="/img/spritemap.svg#immerse"></use></svg>
						</span>
						immerse
					</a>
					<ul class="sub-nav">
						<li><a href="#food">Food</a></li>
						<li><a href="#culture">Culture</a></li>
					</ul>
				</li>
				<li class="main-link-wrap">
					<a href="#become" class="main-link">
						<span class="icon-svg icon-become">
							<svg><use xlink:href="/img/spritemap.svg#become"></use></svg>
						</span>
						become
					</a>
					<ul class="sub-nav">
						<li><a href="#a-student">A Student</a></li>
						<li><a href="#a-faculty-member">A Faculty Member</a></li>
						<li><a href="#a-partner-school">A Partner School</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<a href="#" class="btn-share">
			<span class="icon-svg icon-share">
				<svg><use xlink:href="/img/spritemap.svg#share"></use></svg>
			</span>
			share
		</a>
	</div>
</div>