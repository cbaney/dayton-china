<div class="expand">
	<div class="hero" data-section="expand">
		<div class="copy-wrap play-word" data-audio="/audio/explore">
			<div class="heading" data-scroll-watch data-lazy-bg="true" data-scroll-watch data-lazy-load="/img/anim-expand.png" data-lazy-load-mobile="/img/title-expand.svg" data-animation="true" data-frames="39" data-duration="1.6" data-width="40560"><span class="visuallyhidden">expand</span></div>
			<div class="symbol">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="971.8px" height="480.6px" viewBox="0 0 971.8 480.6" enable-background="new 0 0 971.8 480.6" xml:space="preserve">
					<linearGradient id="expandGradient" gradientUnits="userSpaceOnUse" x1="84.9609" y1="1209.1953" x2="84.9609" y2="-226.8078">
						<stop  offset="0" style="stop-color:#84F44F"/>
						<stop  offset="1" style="stop-color:#D0F435"/>
					</linearGradient>
					<path id="XMLID_27_" fill="url(#expandGradient)" d="M115.2,285.3l50.8-13.7v-50.8c-5.2,1.3-13.7,3.3-25.4,5.9
						c-10.4,2.6-18.9,4.6-25.4,5.9v-87.9h54.7V97.8h-54.7v-84H66.4v84H5.9v46.9h60.5v99.6c-6.5,1.3-16.3,3.3-29.3,5.9
						c-16.9,3.9-29.3,7.2-37.1,9.8l3.9,56.6L66.4,299v105.5c1.3,14.3-6.5,20.8-23.4,19.5c-13,0-24.8,0-35.2,0
						c3.9,14.3,7.1,31.2,9.8,50.8c20.8,0,36.4-0.7,46.9-2c35.2-1.3,52.1-19.5,50.8-54.7V285.3z"/>
					<path id="XMLID_28_" fill="url(#expandGradient)" d="M775.3,190h187v-49h-204c4.5-44.5,5-88,5.5-127.5c8-1,12-4.5,12.5-9.5l-64-4
						c-0.5,44.5,0,93-4.5,141H516.3v49h184.5c-18.5,96-66.5,186-193.5,237.5c13,10.5,27.5,27.5,34.5,40.5c118.5-51,173-135,198.5-225.5
						c37,107.5,96.5,185,197,225.5c7.5-14,23-35,34.5-45.5C866.8,386,806.3,301,775.3,190z"/>
					<path id="XMLID_29_" fill="url(#expandGradient)" d="M380.9,80.3c-13-26-25.4-48.8-37.1-68.4L293,29.5c5.2,9.1,12.4,22.8,21.5,41
						c6.5,11.7,11,20.2,13.7,25.4H197.3v127c0,58.6-3.9,102.2-11.7,130.9c-7.8,31.2-24.1,61.9-48.8,91.8c3.9,2.6,11.7,8.5,23.4,17.6
						c10.4,9.1,17.6,15,21.5,17.6c27.3-37.8,45.6-73.5,54.7-107.4c9.1-33.8,13.7-82,13.7-144.5v-84h224.6V95.9H345.7L380.9,80.3z"/>
				</svg>

				<div class="phonetic">Tansuo</div>
			</div>
		</div>
	</div>

	<div data-section="academics">

		<div class="view quote-wrap dark-overlay" data-scroll-watch data-lazy-load="/img/_temp-student3.jpg" data-lazy-load-mobile="/img/_temp-student3.jpg" data-lazy-bg="true">
			<div class="copy-wrap content-wrap">
				<div class="copy">
					<p>When I first thought about China, I thought more about the ancient history&hellip; and so I expected to see more of the cultural things and not such a modern city. <em class="title">Lauren Schmitz, Sophomore, Civil Engineering, Spring 2015</em></p>
				</div>
			</div>
		</div>

		<div class="view fluid v-padding-extra">
			<div class="container">
				<div class="toggle-view-wrap">

					<div class="row">
						<div class="col-sm-12">
							<div class="dropdown dropdown-toggle">
								<div class="dropdown-selected">
									<span class="dropdown-selected-copy">Selected</span>
									<div class="icon-arw-wrap">
										<span class="icon-svg icon-arw">
											<svg><use xlink:href="/img/spritemap.svg#arw-solid"></use></svg>
										</span>
									</div>
								</div>
								<ul class="toggle">
									<li><a href="#expand-spring-2016">Spring 2016</a></li>
									<li><a href="#expand-summer-2016">Summer 2016</a></li>
									<li><a href="#expand-inb-352">INB 352 Spring 2016</a></li>
									<li><a href="#expand-mee-499">MEE 499</a></li>
									<li><a href="#expand-ece-499">ECE 499</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="toggle-view" id="expand-spring-2016">

						<div class="toggle-piece">
							<h2>Make the World Your Classroom</h2>

							<p>Make yourself more marketable — jet to China to study for 14 weeks during the spring 2016 semester. At the University of Dayton China Institute, you'll gain an international perspective for today's job market. As you learn from UD professors, international professionals and prominent leaders in innovation, you'll advance your resume — and enrich your future career.</p>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Dates
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-8">
										<p class="kicker">The China Institute Spring 2016 Semester Abroad Program will run from January 14 to April 25, 2016. This timeline contains two six-week academic sessions and two mid-term breaks.</p>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<ul class="bullet">
											<li>Thursday, January 14 - Depart Dayton for Shanghai</li>
											<li>Friday, January 15 - Arrive in Shanghai</li>
											<li>Monday, January 18 - Academic Session 1 Begins</li>
											<li>Saturday-Sunday, February 6-14 - Chinese New Year Break</li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="bullet">
											<li>Friday, March 4 - Academic Session 1 Ends</li>
											<li>Monday, March 7 - Academic Session 2 Begins</li>
											<li>Thursday-Monday, March 24-28 - Easter Break</li>
											<li>Wednesday, April 20 - Academic Session 	2 Ends (after last class)</li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="bullet">
										<li>The spring program will end with a Beijing tour. Students will depart from Beijing on Monday, April 25 and arrive in the United States on the same day (due to the time change) — you’ll be back early for summer!</li></ul>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Cost
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-6">
										<p>We live and work in a world without borders, and employers are looking for global leaders. We want to help you gain a global experience — and an edge in the workplace. That’s why we consider all students not currently registered at the University of Dayton for up to $15,000 in scholarships to help cover the cost of tuition, your international airfare and visa, prearranged housing, meals and tours of a few of China’s most spectacular cities.</p>

										<ul class="bullet">
											<li>Tuition&mdash;$19,545</li>
											<li>Housing, meals, international airfare, visa costs, excursions and tours&mdash;$9,095</li>
											<li>Total program costs&mdash;$28,640</li>
										</ul>
									</div>
									<div class="col-sm-6">
										<p>
											UD students are eligible for a $3,000 travel abroad scholarship, which will help cover the cost of your international airfare, visa costs, and excursions/tours. If you are a current UD student, you can study for a semester at the China Institute at the same cost as one on-campus semester at UD.
										</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Course Listing
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-8">
										<p class="kicker">You may choose to take up to 18 credit hours from the 19 courses* offered across the College of Arts and Sciences, School of Business Administration and School of Engineering.**</p>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<h3>College of Arts and Sciences</h3>
										<ul class="bullet">
											<li>ASI 341: Special Topics in Arts Study: Chinese Language and Culture (3 credits)***</li>
											<li>HST 103: The West and the World (3 credits)</li>
											<li>HST 252: American History Since 1865 (3 credits)</li>
											<li>HST 376: Social and Cultural History of the United States (3 credits)</li>
											<li>PHL 103: Introduction to Philosophy (3 credits)</li>
											<li>PHL 312/313: Ethics/Business Ethics (3 credits)</li>
											<li>SOC 101: Principles of Sociology (3 credits)</li>
											<li>SOC 435: Globalization (3 credits)</li>
											<li>SSC 200: Social Science Integrated (3 credits)</li>
										</ul>
									</div>
									<div class="col-sm-4">
										<h3>School of Business Administration#</h3>
										<ul class="bullet">
											<li>ECO 203: Principles of Microeconomics (3 credits)</li>
											<li>ECO 460: Economic Growth and Development (3 credits)</li>
											<li>MGT 300/301: Organizational Behavior (3 credits)****</li>
											<li>INB 302: The Real-World Classroom: Introduction to International Business (3 credits)</li>
											<li>MKT 300/301: Survey/Principles of Marketing (3 credits)*****</li>
										</ul>
									</div>
									<div class="col-sm-4">
										<h3>School of Engineering±</h3>
										<ul class="bullet">
											<li>EGR 201: Engineering Mechanics (3 credits)</li>
											<li>EGR 202: Engineering Thermodynamics (3 credits)</li>
											<li>EGR 203: Electrical and Electronic Circuits (3 credits)</li>
											<li>EGR 203L: Electrical and Electronic Circuits Laboratory (1 credit)</li>
											<li>EGR/IET 323: Project Management (3 credits)</li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<ul>
											<li>*Courses may be subject to change.</li>
											<li>**Students should discuss study plans with their adviser before applying for the program.</li>
											<li>***This course is mandatory for all student requiring a visa to study in China.</li>
											<li>****MGT 300 for non-business majors only; MGT 301 for business majors only.</li>
											<li>*****MKT 300 for non-business majors only; MKT 301 for business majors only. </li>
											<li>#The University of Dayton School of Business Administration is accredited by AACSB - The Association of Advance Collegiate Schools of Business.</li>
											<li>±The University of Dayton School of Engineering is ABET accredited.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="toggle-view" id="expand-summer-2016">

						<div class="toggle-piece">
							<h2>Get Global with Your Education.</h2>

							<p>Make yourself more marketable &mdash; jet to China for six weeks during the first summer session. At the University of Dayton China Institute, you'll gain an international perspective for today's job market. As you learn from UD professors, international professionals and prominent leaders in innovation, you'll advance your résumé &mdash; and enrich your future career. </p>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Dates
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p class="kicker">The China Institute Summer 2016 Program will run from May 12 to June 26, 2016 (summer session 1).</p>
										<p>You will fly from the U.S. to Shanghai and end with a trip to Beijing from June 23-25. Students will depart Beijing on June 26 and arrive in the United States the same day due to the time change.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Cost
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p class="kicker">We live and work in a world without borders, and employers are looking for global leaders. WE want to help you gain a global experience - and an edge in the workplace. That's why we are providing a <strong>$3,000 scholarship</strong> to help cover your international airfare, visa costs, housing and tours of a few of China's most spectacular sights.</p>
										<ul class="bullet">
											<li>Total Program Cost: $15,500 (includes tuition for 9 or 10 credit hours, airfare, Chinese visa, tours and excursions, housing and transportation costs)</li>
											<li>Net Price after Scholarship: $12,500</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Course Listing
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-8">
										<p class="kicker">You may choose three courses (four, if taking both EGR 203 and EGR 203L/ECE 201L) from the 12 courses* offered through the College of Arts and Sciences, School of Business Administration and School of Engineering.**</p>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<h3>College of Arts and Sciences</h3>
										<ul class="bullet">
											<li>ANT 150: Cultural Anthropology (3 credits)</li>
											<li>HST 332: Modern China (3 credits)</li>
											<li>MTH 169: Analytic Geometry and Calculus II (4 credits)</li>
											<li>PHL 312/313: Ethics/Business Ethics (3 credits)</li>
											<li>SSC 200: Social Science Integrated (3 credits)</li>
										</ul>
									</div>
									<div class="col-sm-4">
										<h3>School of Business Administration†</h3>
										<ul class="bullet">
											<li>ECO 203: Principles of Microeconomics (3 credits)</li>
											<li>ECO 460: Economic Growth and Development (3 credits)</li>
											<li>FIN 301: Introduction to Financial Management (3 credits)</li>
											<li>MKT 300/301: Survey/Principles of Marketing (3 credits)***</li>
										</ul>
									</div>
									<div class="col-sm-4">
										<h3>School of Engineering±</h3>
										<ul class="bullet">
											<li>EGR 201: Engineering Mechanics (3 credits)</li>
											<li>EGR 202: Engineering Thermodynamics (3 credits)</li>
											<li>EGR 203: Electrical and Electronic Circuits (3 credits)</li>
											<li>EGR 203L/ECE 201L: Electrical and Electronic Circuits Lab (1 credit)</li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<ul>
											<li>*﻿Courses may be subject to change.</li>
											<li>**Students should discuss study plans with their academic adviser before applying to the program.</li>
											<li>***MKT 300 for non-business majors only; MKT 301 for business majors only.</li>
											<li>†﻿The University of Dayton School of Business Administration is accredited by AACSB - The Association of Advance Collegiate Schools of Business.</li>
											﻿<li>±The University of Dayton School of Engineering is ABET accredited.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>


					</div>

					<div class="toggle-view" id="expand-inb-352">

						<div class="toggle-piece">
							<h2>INB 352 Intersession Program</h2>

							<p>The School of Business Administration is committed to global learning.&nbsp; That’s why they are partnering with the University of Dayton China Institute to provide students a two-week, education-abroad opportunity in China, held during the spring 2016 intersession.</p>
							<p>Over the two-week period, students will take an intensive business course - INB 352: "Doing Business in China: Prospects and Challenges in the Middle Kingdom" - while exploring the rich cultures of Beijing, Shanghai and Suzhou. &nbsp;As an added bonus, students will have the opportunity to participate in corporate site visits in each major city. <em>All majors are welcome and encouraged to apply.</em></p>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Tours and Excursions
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>While visiting China, students will experience three major Chinese cities. Students will begin their journey in Beijing, a city of more than 2,500 years of history and China’s foremost political and cultural center. Students will then travel to Suzhou and visit Suzhou Industrial Park, known as China’s ‘Silicon Valley’ and home to the University of Dayton China Institute and one-third of the world’s Fortune 500 companies. Students will end their trip in Shanghai, one of the largest metropolitan areas in the world.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Course Description
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>This two-week course is an intensive study of business issues in China. Prior to arriving in China, students will be required to complete about 20 hours of classwork (during mid-fall) in preparation for this education abroad experience. Additionally, students must attend post-trip summary classes. This course will adopt a combination of classroom lectures and site visits to businesses and governmental agencies. Classroom lectures, excursions, corporate tours and other activities will provide students a context in which to learn about China’s business environment and culture.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Prerequisites
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>Students must be in sophomore standing or higher, have at least a 2.75 cumulative GPA, be in good academic and behavioral standing with the University of Dayton and have an interest in studying abroad.  No prior international travel experience is required. Students do not need to have completed INB 302: “Survey of International Business” in order to participate. Students who have previously taken INB 352 are also eligible for this program.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Dates and Cost
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>The spring 2016 intersession program will take place January 2-14, 2016. Students depart from the United States on Saturday, January 2 and arrive in Beijing on Sunday, January 3. Students depart from Shanghai on Thursday, January 14 and arrive in the United States the same day (due to the time change).</p>
										<p>The total program cost for the spring 2016 intersession is $5,200, which includes tuition, international airfare and visa costs, housing and accommodations, meals, transportation and associated tours and excursions.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Application Process
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>The application deadline for this intersession program is <strong>Friday, October 16, 2015.</strong> You may apply for the program at any time. When applying, please make sure to select "Spring", "2016" and "Intersession" program in the appropriate fields on the application form. You may access the application form by clicking <a href="https://udayton.edu.185r.net/application/login/" target="_blank">here</a>.</p>
										<p>For questions regarding the spring 2016 intersession, please contact Professor Terence Lau, Associate Dean of Undergraduate Programs, at <a href="mailto:tlau01@udayton.edu">tlau01@udayton.edu</a>.</p>
									</div>
								</div>
							</div>
						</div>


					</div>

					<div class="toggle-view" id="expand-mee-499">

						<div class="toggle-piece">
							<h2>MEE 499 Intersession Program</h2>

							<p>The School of Engineering is committed to global learning. That's why they are partnering with the University of Dayton China Institute to provide students a two-week, education abroad opportunity in China, held during the spring 2016 intersession.</p>

							<p>Over the two-week period, students will take an intensive engineering course - MEE 499: "Robot Motion Analysis and Kinematic Synthesis of Machines" - while exploring the rich cultures of Beijing, Suzhou and Shanghai. To complement the academic and cultural component of this experience, students will participate in corporate site visits. <em>This program is open to Mechanical, Electrical, Computer and Civil Engineering students with a minimum of 60 credit hours completed by January 1, 2016. 75 credit hours completed is preferred.</em></p>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Tours and Excursions
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>While visiting China, students will experience three major Chinese cities. Students will begin their journey in Beijing, a city of more than 2,500 years of history and China’s foremost political and cultural center. Students will then travel to Suzhou and visit Suzhou Industrial Park, known as China’s ‘Silicon Valley’ and home to the University of Dayton China Institute and one-third of the world’s Fortune 500 companies. Students will end their trip in Shanghai, one of the largest metropolitan areas in the world.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Course Description
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>This is a course about studying and designing things that move. The goal is to arrive at both an intuitive and theoretically sound approach to describing, analyzing and designing various mechanical systems. The course is comprised of four significant parts. The first part is concerned with sketching (on paper or in CAD) and answers questions about planar motion from understanding basic geometry. The second part presents the mathematical theory used to describe motion, called kinematics. The third part is the study of robot kinematics. This includes developing the relationships between the joint positions and motions of the robot and the resulting gripper locations and velocities. The fourth part uses kinematics to help synthesize planar mechanisms.</p>
										<p><em>This course counts as a technical elective for Mechanical Engineering, Electrical Engineering, Computer Engineering or Civil Engineering, toward the Minor in Mechanical Systems and toward the Concentration in Robotics in Electrical Engineering or Computer Engineering.﻿</em></p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Prerequisites
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>Students must have completed a minimum of 60 credit hours by January 1, 2016 (75 credit hours completed is preferred), have at least a 2.5 cumulative GPA, be in good academic and behavioral standing with the University of Dayton and have an interest in studying abroad. No prior international travel experience is required.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Dates and Cost
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>The spring 2016 intersession program will take place January 2-14, 2016. Students depart from the United States on Saturday, January 2 and arrive in Beijing on Sunday, January 3. Students depart from Shanghai on Thursday, January 14 and arrive in the United States on the same day (due to the time change).</p>
										<p>The total program cost for this intersession program is $4,700, which includes tuition, international airfare and visa costs, housing and accommodations, meals, transportation and associated tours and excursions.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Application Process
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>The deadline for this intersession program is <strong>Friday, October 16</strong>. You may apply for the program at any time. When applying, please make sure to select "Spring", "2016" and "MEE 499" in the appropriate fields on the application form. You may access the application form by clicking <a href="https://www.udayton.edu/china_institute/academic_programs/apply_now.php" target="_blank">here</a>.</p>
										<p>For questions regarding the MEE 499 Intersession Program, please contact Dr. Andrew Murray at <a href="mailto:amurray1@udayton.edu">amurray1@udayton.edu</a>.</p>
									</div>
								</div>
							</div>
						</div>


					</div>

					<div class="toggle-view" id="expand-ece-499">

						<div class="toggle-piece">
							<h2>ECE 499 Intersession Program</h2>

							<p>The School of Engineering is committed to global learning. That's why they are partnering with the University of Dayton China Institute to provide students a two-week, education abroad opportunity in China, held during the spring 2016 intersession.</p>

							<p>Over the two-week period, students will take an intensive engineering course - ECE 499: "Contemporary Digital Systems Design" - while exploring the rich cultures of Beijing, Suzhou and Shanghai. To complement the academic and cultural component of this experience, students will participate in corporate site visits. <em>This program is open to Engineering students with Junior and Senior status.</em></p>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Tours and Excursions
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>While visiting China, students will experience three major Chinese cities. Students will begin their journey in Beijing, a city of more than 2,500 years of history and China’s foremost political and cultural center. Students will then travel to Suzhou and visit Suzhou Industrial Park, known as China’s ‘Silicon Valley’ and home to the University of Dayton China Institute and one-third of the world’s Fortune 500 companies. Students will end their trip in Shanghai, one of the largest metropolitan areas in the world.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Course Description
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>This course is a tutorial in modern design of digital systems. It consists of a thorough analysis of good digital design practices, proper testbench design and execution and digital system prototyping using Field-Programmable Gate Arrays (FPGAs). This course also includes an introduction to the Very High Speed Integrated Circuit (VHSIC) Hardware Description Language (VHDL). Students will be required to design and implement several digital systems designs.</p>
										<p><em>This course counts as a technical elective in either Mechanical Engineering, Electrical Engineering, Computer Engineering or Civil Engineering and toward the 5 year Bachelor's Plus Master's (BPM) program in Electrical Engineering and Computer Engineering.</em></p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Prerequisites
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>Students must be in junior or senior standing, have at least a 2.5 cumulative GPA, be in good academic and behavioral standing with the University of Dayton and have an interest in studying abroad. No prior international travel experience is required.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Program Dates and Cost
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>The spring 2016 intersession program will take place January 2-14, 2016. Students depart from the United States on Saturday, January 2 and arrive in Beijing on Sunday, January 3. Students depart from Shanghai on Thursday, January 14 and arrive in the United States on the same day (due to the time change).</p>
										<p>The total program cost for this intersession program is $4,700, which includes tuition, international airfare and visa costs, housing and accommodations, meals, transportation and associated tours and excursions.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="accordion-wrap toggle-piece">
							<h2>
								<a href="#" class="btn-accordion">
									Application Process
									<span class="icon-svg icon-accordion">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32.6px" height="32.6px" viewBox="0 0 32.6 32.6">
											<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="16.3" cy="16.3" r="15.8"/>
											
											<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="8.4" y1="16.3" x2="24.2" y2="16.3"/>
											<g class="vert-line">
												<line fill="none" stroke="currentColor" stroke-miterlimit="10" x1="16.3" y1="24.2" x2="16.3" y2="8.4"/>
											</g>
										</svg>
									</span>
								</a>
							</h2>
							<div class="accordion-content">
								<div class="row">
									<div class="col-sm-12">
										<p>The deadline for this intersession program is<strong>Friday, October 16</strong>. You may apply for the program at any time. When applying, please make sure to select "Spring", "2016" and "ECE 499" in the appropriate fields on the application form. You may access the application form by clicking <a href="https://www.udayton.edu/china_institute/academic_programs/apply_now.php" target="_blank">here</a>.</p>
										<p>For questions regarding the ECE 499 Intersession Program, please contact Dr. Eric Balster at <a href="mailto:ebalster1@udayton.edu">ebalster1@udayton.edu</a>.</p>
									</div>
								</div>
							</div>
						</div>


					</div>


				</div>
			</div>
		</div>
	</div>



	<div class="view university-partnerships" data-section="university-partnerships">
		<div class="container">
			<div class="toggle-view-wrap">

				<ul class="toggle">
					<li><a href="#partnerships-intro">Partnerships</a></li>
					<li><a href="#partnerships-corporate">Corporate Partners</a></li>
					<li><a href="#partnerships-university">University Partners</a></li>
				</ul>

				<div class="toggle-view" id="partnerships-intro">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 toggle-piece">
							<p>Our exclusive partnerships with international universities and Fortune 500 companies means you’ll be joining the future of innovation the day you arrive. With our established relationships, you’ll have the chance to learn through firsthand experience at some of the world’s most successful companies. Enjoy seminars on Chinese culture from researchers at multinational companies. Participate in projects with professors from partner institutions. Shape your own international experience and add to your growing global perspective.</p>
						</div>
					</div>
				</div>

				<div class="toggle-view partner-grid" id="partnerships-corporate">
					<div class="logo-row toggle-piece">
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-crown.png" data-lazy-load-mobile="/img/corp-partner-crown.png" alt="Crown logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-csa-group.png" data-lazy-load-mobile="/img/corp-partner-csa-group.png" alt="CSA Group logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-marian.png" data-lazy-load-mobile="/img/corp-partner-marian.png" alt="Marian logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-emerson.png" data-lazy-load-mobile="/img/corp-partner-emerson.png" alt="Emerson logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-johnson-and-johnson.png" data-lazy-load-mobile="/img/corp-partner-johnson-and-johnson.png" alt="Crown logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-ge.png" data-lazy-load-mobile="/img/corp-partner-ge.png" alt="ge logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-henny-penny.png" data-lazy-load-mobile="/img/corp-partner-henny-penny.png" alt="henny-penny logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-fuyao.png" data-lazy-load-mobile="/img/corp-partner-fuyao.png" alt="fuyao logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-lilly.png" data-lazy-load-mobile="/img/corp-partner-lilly.png" alt="lilly logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-makino.png" data-lazy-load-mobile="/img/corp-partner-makino.png" alt="makino logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-sas.png" data-lazy-load-mobile="/img/corp-partner-sas.png" alt="sas logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-delphi.png" data-lazy-load-mobile="/img/corp-partner-delphi.png" alt="delphi logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-veri-silicon.png" data-lazy-load-mobile="/img/corp-partner-veri-silicon.png" alt="veri-silicon logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-lmw.png" data-lazy-load-mobile="/img/corp-partner-lmw.png" alt="lmw logo">
						</div>
						<div class="logo-wrap">
							<img data-scroll-watch data-lazy-load="/img/corp-partner-honeywell.png" data-lazy-load-mobile="/img/corp-partner-honeywell.png" alt="honeywell logo">
						</div>
					</div>
				</div>


			</div>
		</div>

		<div class="bg-video-wrap dark-overlay" data-scroll-watch data-lazy-load-mobile="/img/bg-university-partnerships.jpg" data-lazy-bg="true">
			<video autoplay muted loop data-scroll-watch data-lazy-load="video/UDAY_CHINALOOPS_GreaterChinaRC01.mp4"></video>
		</div>

	</div>

</div>