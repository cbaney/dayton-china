<div class="immerse">
	<div data-section="immerse">
		<div class="hero">
			<div class="copy-wrap">
				<div class="heading" data-scroll-watch data-lazy-bg="true" data-scroll-watch data-lazy-load="/img/anim-immerse.png" data-lazy-load-mobile="/img/title-immerse.svg" data-animation="true" data-frames="48" data-duration="1.96" data-width="49920"><span class="visuallyhidden">immerse</span></div>
				<div class="symbol">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="972px" height="466px" viewBox="0 0 972 466" enable-background="new 0 0 972 466" xml:space="preserve">
					<g>
						<defs>
							<path id="immerse-path" d="M26,137.5c31.5,11.5,72.5,32,92.5,48.5L91,224c-19.5-16.5-59-39-91-52.5L26,137.5z M15.5,427
								C43,387,83,320,113,260.5l35.5,31.5C121,346.5,87,406.5,61.5,447c1.5,3,2.5,5.5,2.5,8c0,3-1,5.5-2.5,7.5L15.5,427z M58,2.5
								c31,14,69.5,37.5,88,55.5l-30.5,36.5c-17.5-19-55-44-86-59.5L58,2.5z M392.5,393c0,17,1,19.5,8.5,19.5h19c8.5,0,10-13.5,11-80.5
								c10,8,29,16.5,42,20c-4,80-14.5,104.5-48.5,104.5H392c-35,0-46.5-14-46.5-63.5V188.5H261V256c0,67.5-15,155-96,209
								c-6-10-22.5-29.5-32-36c73-48,81.5-116.5,81.5-174V144h178V393z M201,65.5V146h-45V20.5h300V146h-47V65.5H201z M526,136
								c30,11.5,68.5,32,87.5,48.5L586,224c-18-17-55.5-39-86.5-52.5L526,136z M513,430.5c23.5-39,57-104.5,82-162.5l37,28.5
								c-22,53-50,111.5-72,150.5c2.5,3,3.5,6,3.5,8.5s-1,5-2,7L513,430.5z M552.5,0c30,13,68.5,34.5,87,52c-12,16.5-20,27.5-28,39
								C594,73.5,556,49.5,526,35.5L552.5,0z M917.5,289.5L916,291c-17,40.5-43.5,72.5-76.5,98c37.5,17.5,82,29.5,132.5,35.5
								c-10,10-22,28.5-28,41.5c-56.5-9.5-105.5-26-145.5-50c-43,24-93.5,40-147.5,50c-4-11.5-14-29.5-22.5-39.5c47.5-7,92.5-19,131.5-38
								c-22.5-20-41.5-43.5-56.5-70.5l22.5-7h-44v-19.5h-39.5v-90h317v88H917.5z M924,14v162H673v-36h205.5v-28h-189V77.5h189V50H677V14
								H924z M914,239.5H686v34h196l8-1.5l24,10V239.5z M747.5,311c13,21,30.5,39,52,54.5C821,350.5,840,332,854,311H747.5z"/>
						</defs>
						<clipPath id="immerse-clip">
							<use xlink:href="#immerse-path"  overflow="visible"/>
						</clipPath>
						<linearGradient id="immerse-gradient" gradientUnits="userSpaceOnUse" x1="484.9232" y1="1199.8807" x2="484.9232" y2="-236.1225">
							<stop  offset="0" style="stop-color:#84F44F"/>
							<stop  offset="1" style="stop-color:#D0F435"/>
						</linearGradient>
						<rect x="-155.1" y="-169" clip-path="url(#immerse-clip)" fill="url(#immerse-gradient)" width="1280" height="800"/>
					</g>
					</svg>

					<div class="phonetic">Tansuo</div>
				</div>
			</div>
		</div>

	</div>

	<div data-section="food">

		<div class="view video-cta-wrap dark-overlay">
			<div class="copy-wrap content-wrap">
				<a href="#">
					<span class="icon-svg icon-play">
						<svg><use xlink:href="/img/spritemap.svg#play"></use></svg>
					</span>
					<p>Watch Food Video</p>
				</a>
			</div>
			<div class="bg-video-wrap dark-overlay" data-scroll-watch data-lazy-bg="true" data-lazy-load-mobile="/img/bg-food-video.jpg">
				<video autoplay muted loop data-scroll-watch data-lazy-load="video/UDAY_CHINALOOPS_FoodRC01.mp4"></video>
			</div>
		</div>

		<div class="view gallery-wrap">
			<div class="gallery gallery-food">
				<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery-food.jpg" data-lazy-load-mobile="/img/_temp-gallery-food.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-food.jpg">
					<div class="copy">
						<h2>LABS &amp; FACILITIES</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
				<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery-food.jpg" data-lazy-load-mobile="/img/_temp-gallery-food.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-food.jpg">
					<div class="copy">
						<h2>LABS &amp; FACILITIES</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
				<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery-food.jpg" data-lazy-load-mobile="/img/_temp-gallery-food.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-food.jpg">
					<div class="copy">
						<h2>Another Heading</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
				<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery-food.jpg" data-lazy-load-mobile="/img/_temp-gallery-food.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-food.jpg">
					<div class="copy">
						<h2>Third Heading</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="view" data-section="culture">
		<div class="container carousel-wrap">
			<button class="arw-carousel prev">
				<span class="icon-svg icon-arw-left">
					<svg><use xlink:href="/img/spritemap.svg#arw-left"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
			<div class="row">

				<div class="col-sm-6 col-sm-offset-3">
					<div class="carousel">
						<div class="slide">
							<div class="slide-content">
								<div class="row">
									<div class="col-sm-12"><img data-scroll-watch data-lazy-load="img/_temp-infographic.svg" alt="" class="img-full infographic"></div>
									<ul class="block-list">
										<li class="col-sm-6">
											Suzhou Industrial Park is a 25-year-old city of the future
										</li>
										<li class="col-sm-6">
											Suzhou has been around 100 times longer, boasting 2,500 years of ancient history
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="slide">
							<div class="slide-content">
								<div class="row">
									<ul class="block-list">
										<li class="col-sm-6">
											Suzhou Industrial Park is a 25-year-old city of the future
										</li>
										<li class="col-sm-6">
											Suzhou has been around 100 times longer, boasting 2,500 years of ancient history
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="slide-count-wrap"><span class="slide-count">1</span>/2</div>
				</div>

			</div>
			<button class="arw-carousel next">
				<span class="icon-svg icon-arw-right">
					<svg><use xlink:href="/img/spritemap.svg#arw-right"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
		</div>
		<div class="bg-video-wrap dark-overlay">
			<video autoplay muted loop data-scroll-watch data-lazy-load="video/UDAY_CHINALOOPS_CultureRC03.mp4"></video>
		</div>
	</div>

</div>