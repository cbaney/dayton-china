<div class="meet">
	<div class="hero" data-section="meet">
		<div class="copy-wrap">
			<div class="heading" data-scroll-watch data-lazy-bg="true" data-scroll-watch data-lazy-load="/img/anim-meet.png" data-lazy-load-mobile="/img/title-meet.svg" data-animation="true" data-frames="33" data-duration="1.32" data-width="34320"><span class="visuallyhidden">meet</span></div>
			<div class="symbol">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="961.5px" height="459.5px" viewBox="0 0 961.5 459.5" enable-background="new 0 0 961.5 459.5" xml:space="preserve">
				<g>
					<defs>
						<path id="meet-path" d="M112,187.5V352c34,55,100.5,55,189,55c57.5,0,126.5-1.5,170.5-5c-5.5,11-12.5,33-15,46.5
							C419,450,362,451,310.5,451c-109,0-172,0-218.5-59.5c-21.5,21-43.5,41.5-62,57.5c0,5-1.5,7-5.5,9.5L0,408c20-14,45-33.5,66.5-52.5
							v-124H5v-44H112z M45,0c30,23,65.5,57,81,81.5l-38.5,30C73,86.5,39,51,9,26.5L45,0z M193.5,240v144h-43V202H273v-27H172.5V6.5h249
							V175H316v27h132v150c0,38-17,40-87,40c-2-10.5-8-24-12.5-33c21,0.5,41.5,0.5,47.5,0.5c6-0.5,8-2.5,8-7.5V240h-90.5v52l34.5-1.5
							c-4-10.5-8.5-20.5-12.5-29.5l29.5-7c12.5,25,25,59,29.5,79.5l-31,8.5c-1-6-3-13-5-20.5c-51,5.5-101,10-134.5,12.5
							c-1,4-4,6.5-7.5,7l-14-43c19.5-1,44-2,70.5-3.5V240H193.5z M216,41v33.5h57V41H216z M216,141h57v-34h-57V141z M376.5,74.5V41H316
							v33.5H376.5z M376.5,141v-34H316v34H376.5z M660.5,306.5V373c35.5-5,72.5-10,108-15.5l2.5,41c-88,15.5-182.5,31-243,40
							c-1,4.5-4,7.5-7.5,8.5L498,394.5c31-3.5,71-9,115.5-15v-73H525v-43h88.5v-57L672,210c-0.5,4.5-3.5,7-11.5,8.5v45H748v43H660.5z
							 M642.5,66c-2.5,1.5-6,2.5-10,2.5c-9,26.5-23,61.5-36.5,91c30.5-1,64-2,98-3.5c-12.5-16.5-26-33-38.5-47l34-20
							c31,32.5,67,78,83,108l-37,23.5c-4-8-9-17-15-26.5c-70.5,5-142.5,9.5-189,12c-1,4-4.5,7-8,7.5l-18.5-51l43-1.5
							c13-28,26-64.5,35-95h-78V23.5h263.5V66H642.5z M843,55.5c-0.5,4-3.5,6.5-11.5,8V334H786V52L843,55.5z M903.5,5l58,3
							c-0.5,5-3,7.5-11.5,8.5V399c0,53-19.5,60.5-116,60.5c-2-13.5-9-35.5-16-49c32.5,1,63.5,1,73.5,1c9-0.5,12-3,12-12.5V5z"/>
					</defs>
					<clipPath id="meet-clip">
						<use xlink:href="#meet-path"  overflow="visible"/>
					</clipPath>
					<linearGradient id="meet-gradient" gradientUnits="userSpaceOnUse" x1="480.75" y1="1198.625" x2="480.75" y2="-237.3781">
						<stop  offset="0" style="stop-color:#84F44F"/>
						<stop  offset="1" style="stop-color:#D0F435"/>
					</linearGradient>
					<rect x="-159.2" y="-170.2" clip-path="url(#meet-clip)" fill="url(#meet-gradient)" width="1280" height="800"/>
				</g>
				</svg>

				<div class="phonetic">Tansuo</div>
			</div>
		</div>
	</div>

	<div class="view v-padding fluid" data-section="our-students">
		<div class="container">
			<div class="toggle-view-wrap">
				<div class="row">
					<div class="col-sm-12">
						<ul class="toggle">
							<li><a href="#meet-spring-2015">Spring 2015</a></li>
							<li><a href="#meet-summer-2015">Summer 2015</a></li>
						</ul>
					</div>
				</div>

				<div class="toggle-view" id="meet-spring-2015">

					<h2 class="js-visuallyhidden">Spring 2015</h2>

					<div class="profile-wrap">
						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="/img/student-lauren-schmitz.jpg" data-lazy-load-mobile="/img/student-lauren-schmitz.jpg" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“It was a great experience to step out of my comfort zone by sitting with students I didn't know and trying all of the foods I would normally refrain from.”
											<em class="title">–Lauren Schmitz, Sophomore, Civil Engineering</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“In addition they reassured our decision in traveling abroad for this integrating experience where the international and domestic students learn a lot about each other’s lifestyle and culture, by calling us “young ambassadors” and applauding us for our efforts to become more globally interconnected.”
											<em class="title">–Vidyaarthi Pugalenthi, Freshman, Marketing</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“From the museum we traveled to our final stop, Pingjiang Road. This road is known for being a great place to go to shop. If one were to picture a Chinese market version of an American strip mall they wouldn't be too far off from Pingjiang Road.”
											<em class="title">–Daniel Sheldon, Freshman, Biology</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="/img/student-rachel-craighead.jpg" data-lazy-load-mobile="/img/student-rachel-craighead.jpg" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“When offered the chance to climb the mountain and enter the Basilica, I signed at the first chance.  I have been raised Catholic my entire life, and I have always felt an interconnectedness with God while in nature. The entire place felt peaceful, and I could almost feel the history that had happened in this place.”
											<em class="title">–﻿Rachel Craighead, Freshman, Discover Arts</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“At the end of the day I felt as if I had a greater understanding of the functionality of both Higer and Black and Decker corporations.”
											<em class="title">–﻿Daniel Sheldon, Freshman, Biology</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“It takes a while to climb all the stairs leading to the top, but once you reach it you realize the hard work is totally worth it. The peak offers a view of the mountain below and lets you see into the distance above the clouds. Huangshan Mountain is absolutely gorgeous and is truly a place everyone should visit before they leave China.”
											<em class="title">–﻿Lauren Schmitz, Sophomore, Civil Engineering</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“We went to Shanghai and had a whole day of corporate tours - the U.S. Consulate Commercial Services, Interbrand and Ford. The experience was beyond my expectations.”
											<em class="title">–Jiazhang Chen, Sophomore, Engineering Technology</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“I enjoyed our tours very much. The garden was very beautiful, and the silk factory was educational. I also enjoyed walking around Suzhou.”
											<em class="title">–Andrew Buser, Sophomore, Civil Engineering</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<a href="https://www.udayton.edu/china_institute/experience/spring2015.php" class="cta toggle-piece">See All Spring 2015 Posts</a>
				</div>

				<div class="toggle-view" id="meet-summer-2015">

					<h2 class="js-visuallyhidden">summer 2015</h2>

					<div class="profile-wrap">
						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“It was a great experience to step out of my comfort zone by sitting with students I didn't know and trying all of the foods I would normally refrain from.”
											<em class="title">–Lauren Schmitz, Sophomore, Civil Engineering</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“In addition they reassured our decision in traveling abroad for this integrating experience where the international and domestic students learn a lot about each other’s lifestyle and culture, by calling us “young ambassadors” and applauding us for our efforts to become more globally interconnected.”
											<em class="title">–Vidyaarthi Pugalenthi, Freshman, Marketing</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“From the museum we traveled to our final stop, Pingjiang Road. This road is known for being a great place to go to shop. If one were to picture a Chinese market version of an American strip mall they wouldn't be too far off from Pingjiang Road.”
											<em class="title">–Daniel Sheldon, Freshman, Biology</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“When offered the chance to climb the mountain and enter the Basilica, I signed at the first chance.  I have been raised Catholic my entire life, and I have always felt an interconnectedness with God while in nature. The entire place felt peaceful, and I could almost feel the history that had happened in this place.”
											<em class="title">–﻿Rachel Craighead, Freshman, Discover Arts</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“At the end of the day I felt as if I had a greater understanding of the functionality of both Higer and Black and Decker corporations.”
											<em class="title">–﻿Daniel Sheldon, Freshman, Biology</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“It takes a while to climb all the stairs leading to the top, but once you reach it you realize the hard work is totally worth it. The peak offers a view of the mountain below and lets you see into the distance above the clouds. Huangshan Mountain is absolutely gorgeous and is truly a place everyone should visit before they leave China.”
											<em class="title">–﻿Lauren Schmitz, Sophomore, Civil Engineering</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row toggle-piece">
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“We went to Shanghai and had a whole day of corporate tours - the U.S. Consulate Commercial Services, Interbrand and Ford. The experience was beyond my expectations.”
											<em class="title">–Jiazhang Chen, Sophomore, Engineering Technology</em></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="profile">
									<div class="row">
										<div class="col-sm-4"><img data-scroll-watch data-lazy-load="http://placehold.it/170x170" data-lazy-load-mobile="http://placehold.it/170x170" alt="" class="img-full img-circle"></div>
										<div class="col-sm-8">
											<p>“I enjoyed our tours very much. The garden was very beautiful, and the silk factory was educational. I also enjoyed walking around Suzhou.”
											<em class="title">–Andrew Buser, Sophomore, Civil Engineering</em></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<a href="#" class="cta toggle-piece">See All Summer 2015 Posts</a>
				</div>
			</div>

		</div>
	</div>

	<div data-section="our-faculty-members">
		
		<div class="view quote-wrap dark-overlay" data-scroll-watch data-lazy-load="/img/_temp-quote.jpg" data-lazy-load-mobile="/img/_temp-quote.jpg" data-lazy-bg="true">
			<div class="copy-wrap content-wrap">
				<div class="copy">
					<p>There is not a single day in which walking around Suzhou where you don’t encounter something that is truly fascinating.<em class="title">Sean Wilkinson, Faculty, Art and Design, Spring 2015</em></p>
				</div>
			</div>
		</div>

		<div class="view v-padding fluid">
			<div class="container">
				<div class="toggle-view-wrap">
					<div class="row">
						<div class="col-sm-12">
							<ul class="toggle">
								<li><a href="#meet-our-staff">Our Staff</a></li>
								<li><a href="#meet-past-faculty">Past Faculty</a></li>
							</ul>
						</div>
					</div>

					<div class="toggle-view" id="meet-our-staff">

						<h2 class="js-visuallyhidden">Our Staff</h2>

						<div class="row">
							<div class="col-sm-5 toggle-piece">

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/wei_300.jpg" data-lazy-load-mobile="/img/wei_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Jia Jia Wei</h3>
											<p>Director of China Initiatives - University of Dayton</p>
											<p><a target="_blank" href="https://www.udayton.edu/0/php/contact_form.php?cmsId=26ab842c0a480e99183c97dba0a84502">Email Jia Jia Wei</a></p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/sean-mccarthy.jpg" data-lazy-load-mobile="/img/sean-mccarthy.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Sean McCarthy</h3>
											<p>Assistant Director of China Initiatives - University of Dayton</p>
											<p><a target="_blank" href="https://www.udayton.edu/0/php/contact_form.php?cmsId=26c091320a480e99183c97dbdc7ee18a">Email Sean McCarthy</a></p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/dong_300.jpg" data-lazy-load-mobile="/img/dong_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Dong Zhang</h3>
											<p>Director of Student Programming - China Institute</p>
											<p><a target="_blank" href="https://www.udayton.edu/0/php/contact_form.php?cmsId=26b6e4bd0a480e99183c97db2412e365">Email Dong Zhang</a></p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/kopenski_300.jpg" data-lazy-load-mobile="/img/kopenski_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Mark Kopenski</h3>
											<p>Managing Director - International Markets</p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/deng_300.jpg" data-lazy-load-mobile="/img/deng_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
										<h3>Yanyan Deng (Judy)</h3>
										<p>Office Manager - China Institute</p>
										</div>
									</div>
								</div>

							</div>
							<div class="col-sm-5 toggle-piece">

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/gong_300.jpg" data-lazy-load-mobile="/img/gong_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Zhengyao Gong (George)</h3>
											<p>Assistant to the Executive Director and IT Manager - China Institute</p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/wding_300.jpg" data-lazy-load-mobile="/img/wding_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Wei Ding</h3>
											<p>Program Advisor - China Institute</p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/nding_300.jpg" data-lazy-load-mobile="/img/nding_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Ningning Ding (Peter)</h3>
											<p>Student Advisor - China Institute</p>
										</div>
									</div>
								</div>

								<div class="staff">
									<div class="row">
										<div class="col-xs-4"><img data-scroll-watch data-lazy-load="/img/xin_300.jpg" data-lazy-load-mobile="/img/xin_300.jpg" alt="" class="img-full img-circle" height="132" width="132"></div>
										<div class="col-xs-8">
											<h3>Jige Xin</h3>
											<p>Program Liaison Officer and Lab Specialist - China Institute</p>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>


					<div class="toggle-view" id="meet-past-faculty">

						<h2 class="js-visuallyhidden">Past Faculty</h2>

						<div class="row">
							<div class="col-sm-4 toggle-piece">
								<h3>College Of Arts And Sciences</h3>
								<ul>
									<li>Christopher Agnew, History</li>
									<li>Karen Bartley, History</li>
									<li>Brett Billman, Communication</li>
									<li>Jeremy Forbis, Sociology, Anthropology, and Social Work</li>
									<li>Xiamara Hohman, English</li>
									<li>Xiaoli Li, English</li>
									<li>Kurt Mosser, Philosophy</li>
									<li>Youssef Raffoul, Mathematics</li>
									<li>Cassandra Secrease, Communication</li>
									<li>Versalle Washington, History</li>
									<li>Sean Wilkinson, Art and Design</li>
								</ul>
							</div>
							<div class="col-sm-4 toggle-piece">
								<h3>School Of Business Administration</h3>
								<ul>
									<li>Serdar Durmusoglu, Management and Marketing</li>
									<li>Mark Jacobs, Management Information Systems, Operations Management, and Decision Sciences</li>
									<li>Terence Lau, Office of the Dean, Management and Marketing</li>
									<li>Yue Pan, Management and Marketing, International Business</li>
									<li>Paul Sweeney, Management and Marketing, International Business</li>
									<li>Ting (Jeffrey) Zhang, Economics and Finance</li>
								</ul>
							</div>
							<div class="col-sm-4 toggle-piece">
								<h3>School Of Engineering</h3>
								<ul>
									<li>Eric Balster, Electrical and Computer Engineering</li>
									<li>Philip Doepker, Mechanical and Aerospace Engineering</li>
									<li>Charlie Edmonson, Engineering Technology</li>
									<li>Donald Moon, Electrical and Computer Engineering</li>
									<li>Andrew Murray, Mechanical and Aerospace Engineering</li>
									<li>Denise Taylor, Civil and Environmental Engineering and Engineering Mechanics</li>
								</ul>
							</div>
						</div>
					</div>
	
				</div>

			</div>
		</div>
	</div>

	<div class="view" data-section="the-people-of-suzhou">
		<div class="container carousel-wrap">
			<button class="arw-carousel prev">
				<span class="icon-svg icon-arw-left">
					<svg><use xlink:href="/img/spritemap.svg#arw-left"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="carousel">
						<div class="slide">
							<div class="slide-content">
								<div class="row">
									<div class="col-sm-12"><img data-scroll-watch data-lazy-load="img/_temp-infographic.svg" alt="" class="img-full infographic"></div>
									<ul class="block-list">
										<li class="col-sm-6">
											Suzhou Industrial Park is a 25-year-old city of the future
										</li>
										<li class="col-sm-6">
											Suzhou has been around 100 times longer, boasting 2,500 years of ancient history
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="slide">
							<div class="slide-content">
								<div class="row">
									<ul class="block-list">
										<li class="col-sm-6">
											Suzhou Industrial Park is a 25-year-old city of the future
										</li>
										<li class="col-sm-6">
											Suzhou has been around 100 times longer, boasting 2,500 years of ancient history
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="slide-count-wrap"><span class="slide-count">1</span>/2</div>
				</div>
			</div>
			<button class="arw-carousel next">
				<span class="icon-svg icon-arw-right">
					<svg><use xlink:href="/img/spritemap.svg#arw-right"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
		</div>
		<div class="bg-video-wrap dark-overlay">
			<video autoplay muted loop data-scroll-watch data-lazy-load="video/UDAY_CHINALOOPS_People05.mp4"></video>
		</div>
	</div>

</div>