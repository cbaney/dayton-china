<div class="explore">
	<div class="hero" data-section="explore">
		<div class="copy-wrap play-word" data-audio="/audio/explore">
			<div class="heading" data-lazy-bg="true" data-scroll-watch data-lazy-load="/img/anim-explore.png" data-scroll-watch data-lazy-load-mobile="/img/title-explore.svg" data-scroll-watch data-animation="true" data-frames="41" data-duration="1.64" data-width="42640"><span class="visuallyhidden">Explore</span></div>
			<div class="symbol">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="962px" height="469.5px" viewBox="0 0 962 469.5" enable-background="new 0 0 962 469.5" xml:space="preserve">
				<g>
					<defs>
						<path id="explore-path" d="M157.5,223l5.5,39.5c-17.5,6-35.5,11.5-52.5,17.5v136c0,42-15,54-85.5,53c-1-11.5-7-31.5-13-44
							c10.5,0,20.5,0.5,28.5,0.5c24.5,0,26,0,26-9v-123c-17,5.5-32,10-45,14c-0.5,5-4,7.5-7.5,9l-14-53c18.5-4,41.5-10,66.5-16.5V145H6
							v-43.5h60.5V1l55,2.5c-0.5,4.5-3.5,7-11,8v90H165V145h-54.5v90.5L157.5,223z M464.5,304h-103c29,42.5,73.5,83.5,116,105.5
							C467,418,452,434.5,445,446c-40-25-81-67-111.5-113v135.5H287V335c-31.5,48-74.5,90-120,114.5c-7-11-21-27.5-31-36
							c48-22,94-63.5,123-109.5h-94v-42.5h122v-53l58,3.5c-0.5,4-3.5,7-11.5,8.5v41h131V304z M303.5,81.5c-1,4-4,6.5-10.5,7.5
							c-7,68-24,117-109.5,144c-4.5-10.5-15.5-26-24-34c73.5-20.5,86.5-58.5,91-121.5L303.5,81.5z M215,63v58.5h-40.5v-99h285V123H417
							V63H215z M330.5,75l54,3.5c-0.5,3.5-3,6.5-11,7.5v77c0,11.5,2,13,13,13H421c9.5,0,11.5-4.5,13-35c8,6,25.5,12.5,37,15
							c-4,46-15.5,58-45,58h-46c-40.5,0-49.5-11.5-49.5-50.5V75z M677,379c-2.5,3.5-7.5,4.5-12.5,3.5c-32,29-81,61-123.5,81
							c-8.5-9-24-24-34.5-31.5C549,414.5,597,385.5,626,357L677,379z M834,247.5c42.5,26,97.5,64.5,125,91l-33.5,28
							c-7-7-15.5-15-25.5-23.5c-46.5,1.5-94.5,3.5-140.5,4.5v122H712V349c-65,2-124,3-169.5,4c-1,4.5-4.5,7-8.5,7.5l-17-50
							c29.5,0,66,0,107.5-0.5c11-6,22.5-13,34-20c-25-19-60-41-90-56.5l31-26.5c7.5,3.5,15.5,8,24,12.5c19-15,40.5-34,57-51h-115v61h-45
							V128H712V84H522.5V42.5H712V0l58.5,3.5c-0.5,4-4,7-11.5,8v31h191.5V84H759v44h196.5v101.5h-47v-61H726l18,9.5c-2,3-6.5,4-13,3.5
							c-19,17.5-47,40.5-72.5,58c14.5,8.5,28,17.5,39,25.5c40-26.5,79-55.5,107-80.5l47,27.5c-2,3-7,4-13.5,4c-37,28.5-90,64-140,93.5
							c50-0.5,103.5-1.5,156-2c-17-12.5-35-25-51.5-35L834,247.5z M836,356.5c41.5,21,97,54.5,126,78.5c-17,12.5-28.5,20-39.5,27.5
							c-25.5-24-80-59-123-82L836,356.5z"/>
					</defs>
					<clipPath id="explore-clip">
						<use xlink:href="#explore-path" overflow="visible"/>
					</clipPath>
					<linearGradient id="explore-gradient" gradientUnits="userSpaceOnUse" x1="481" y1="1203.6249" x2="481" y2="-232.3782">
						<stop offset="0" style="stop-color:#84F44F"/>
						<stop offset="1" style="stop-color:#D0F435"/>
					</linearGradient>
					<rect x="-159" y="-165.2" clip-path="url(#explore-clip)" fill="url(#explore-gradient)" width="1280" height="800" class="clip-box"/>
				</g>
				</svg>
				<div class="phonetic">(Tansuo)</div>
			</div>
		</div>
		<div class="bg-dark"></div>
	</div>
	<div class="view" data-section="suzhou">
		<div class="container carousel-wrap">
			<div class="row">

				<div class="col-sm-6 col-sm-offset-3">
					<div class="carousel">
						<div class="slide">
							<div class="slide-content">
								<div class="row">
									<div class="col-sm-12"><img data-scroll-watch data-lazy-load="img/_temp-infographic.svg" alt="" class="img-full infographic"></div>
									<ul class="block-list">
										<li class="col-sm-6">
											Suzhou Industrial Park is a 25-year-old city of the future
										</li>
										<li class="col-sm-6">
											Suzhou has been around 100 times longer, boasting 2,500 years of ancient history
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="slide">
							<div class="slide-content">
								<div class="row">
									<ul class="block-list">
										<li class="col-sm-6">
											Suzhou Industrial Park is a 25-year-old city of the future
										</li>
										<li class="col-sm-6">
											Suzhou has been around 100 times longer, boasting 2,500 years of ancient history
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="slide-count-wrap"><span class="slide-count">1</span>/2</div>
				</div>
			</div>
			<button class="arw-carousel prev">
				<span class="icon-svg icon-arw-left">
					<svg><use xlink:href="/img/spritemap.svg#arw-left"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
			<button class="arw-carousel next">
				<span class="icon-svg icon-arw-right">
					<svg><use xlink:href="/img/spritemap.svg#arw-right"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
		</div>
		<div class="bg-video-wrap dark-overlay">
			<video autoplay muted loop data-scroll-watch data-lazy-load="video/UDAY_CHINALOOPS_SuzhouRC03.mp4"></video>
		</div>
	</div>

	<div class="view gallery-wrap" data-section="campus">
		<div class="container">
			<div class="toggle-view-wrap">

				<ul class="toggle">
					<li><a href="#explore-campus">Campus</a></li>
					<li><a href="#explore-housing">Housing</a></li>
				</ul>

				<div class="toggle-view" id="explore-campus">
					<div class="row">
						<div class="col-sm-3 toggle-piece">

							<h2 class="js-visuallyhidden">Campus</h2>

							<p>The 2-story, on-campus shopping center is loaded with shops, cafes, and a full grocery store for all of the essentials. And you won’t have to venture far for some authentic Chinese cuisine. You can experience the night markets right outside of campus for some adventurous, inexpensive eating.</p>
						</div>

						<div class="gallery gallery-three">
							<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery.jpg" data-lazy-load-mobile="/img/_temp-gallery.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-full.jpg">
								<div class="copy">
									<h2>LABS &amp; FACILITIES</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
							<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery.jpg" data-lazy-load-mobile="/img/_temp-gallery.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-full.jpg">
								<div class="copy">
									<h2>Another Heading</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
							<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery.jpg" data-lazy-load-mobile="/img/_temp-gallery.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery-full.jpg">
								<div class="copy">
									<h2>Third Heading</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="toggle-view" id="explore-housing">
					<div class="row">
						<div class="col-sm-3 toggle-piece">

							<h2 class="js-visuallyhidden">Housing</h2>

							<p>Our quiet campus provides students and faculty with all the comforts of home, including your own apartments with gardens, common areas, laundry rooms on every floor, and a weekly cleaning service. Each of our apartment-style rooms features a full kitchen plus a private bedroom and bathroom.</p>
						</div>

						<div class="gallery gallery2 gallery-three">
							<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery2.jpg" data-lazy-load-mobile="/img/_temp-gallery2.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery2-full.jpg">
								<div class="copy">
									<h2>LABS &amp; FACILITIES</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
							<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery2.jpg" data-lazy-load-mobile="/img/_temp-gallery2.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery2-full.jpg">
								<div class="copy">
									<h2>LABS &amp; FACILITIES</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
							<div class="piece toggle-piece" data-scroll-watch data-lazy-load="/img/_temp-gallery2.jpg" data-lazy-load-mobile="/img/_temp-gallery2.jpg" data-lazy-bg="true" data-img-full="/img/_temp-gallery2-full.jpg">
								<div class="copy">
									<h2>LABS &amp; FACILITIES</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<div class="view" data-section="greater-china">
		<div class="container carousel-wrap">
			<button class="arw-carousel prev">
				<span class="icon-svg icon-arw-left">
					<svg><use xlink:href="/img/spritemap.svg#arw-left"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
			<div class="row">

					<div class="carousel">
						<div class="slide slide-stopwatch">
							<div class="slide-content">
								<h2>From <strong>Suzhou</strong><span class="resp-break"></span> by train</h2>
								<div class="ig-stopwatch" data-scroll-watch data-lazy-bg="true" data-scroll-watch data-lazy-load="/img/ig-stopwatch.png" data-loop="true" data-looptime="1.52" data-animation="true" data-frames="226" data-duration="9" data-width="90400"></div>
								<ul>
									<li>To Shanghai (30-40 min)</li>
									<li>To Nanjing (1.5 hours)</li>
									<li>To Beijing (5 hours)</li>
								</ul>
							</div>
						</div>

					</div>

					<div class="slide-count-wrap"><span class="slide-count">1</span>/2</div>
			</div>
			<button class="arw-carousel next">
				<span class="icon-svg icon-arw-right">
					<svg><use xlink:href="/img/spritemap.svg#arw-right"></use></svg>
				</span>
				<span class="visuallyhidden">previous</span>
			</button>
		</div>
		<div class="bg-video-wrap dark-overlay">
			<video autoplay muted loop data-scroll-watch data-lazy-load="video/UDAY_CHINALOOPS_BirdsRC01.mp4"></video>
		</div>
	</div>
</div>